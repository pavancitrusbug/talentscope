<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('job_id');
            $table->integer('agent_id');
            $table->integer('employer_id');
            $table->tinyInteger('ratting');
            $table->text('review')->nullable()->default(null);
            $table->tinyInteger('status')->default(1)->comment = '0 = invalid | 1 = valid';
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_reviews');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employer_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employers_id')->nullable(); 
            $table->integer('agents_id')->nullable(); 
            $table->enum('friend_status', ['pending', 'approve', 'decline','cancel','sending'])->default('pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('employer_agents');
    }
}

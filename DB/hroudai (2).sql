-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 03:13 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hroudai`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `state_id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ahmedabad', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `specialization_id` int(11) DEFAULT NULL,
  `pin_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_founded` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `user_id`, `logo`, `name`, `address`, `country_id`, `state_id`, `city_id`, `specialization_id`, `pin_code`, `contact`, `email_address`, `about`, `website`, `year_founded`, `verified`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 10, '15471161775c371e91be7db.png', 'citrusbug', NULL, 1, 1, 1, 1, '380013', '9725212073', 'krishna.citrusbug@gmail.com', 'console.log( \"ready!\" );', 'www.citrusbug.com', '2014', 1, NULL, '2019-01-10 04:59:37', '2019-01-10 04:59:37'),
(5, 11, NULL, 'citrusbug', NULL, 1, 1, 1, NULL, '380013', 'Krishna', 'krishna.citrusbug@gmail.com', NULL, 'citrusbug', 'citrusbug', 1, NULL, '2019-01-15 23:46:45', '2019-01-15 23:46:45'),
(6, 12, '15476170325c3ec30840bf0.jpg', 'citrusbug', NULL, 1, 1, 1, NULL, '380013', '9725212073', 'krishna.citrusbug@gmail.com', 'aaaaa', 'www.citrusbug.com', '2018', 1, NULL, '2019-01-16 00:07:12', '2019-01-16 00:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'India', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeetypes`
--

CREATE TABLE `employeetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeetypes`
--

INSERT INTO `employeetypes` (`id`, `name`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Employee type - 01', NULL, 1, NULL, '2019-01-07 06:29:37', '2019-01-10 05:37:37'),
(2, 'ee', NULL, 1, NULL, '2019-01-10 07:47:29', '2019-01-10 07:47:29');

-- --------------------------------------------------------

--
-- Table structure for table `employer_agents`
--

CREATE TABLE `employer_agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `employers_id` int(11) DEFAULT NULL,
  `agents_id` int(11) DEFAULT NULL,
  `friend_status` enum('pending','approve','decline','cancel','sending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `perks&benifits` text COLLATE utf8mb4_unicode_ci,
  `job_category_id` int(11) DEFAULT NULL,
  `employement_type_id` int(11) DEFAULT NULL,
  `job_experience_id` int(11) DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_limit` int(11) DEFAULT NULL,
  `placement_fee` int(11) DEFAULT NULL,
  `fixed_fee` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `title`, `description`, `perks&benifits`, `job_category_id`, `employement_type_id`, `job_experience_id`, `location`, `salary`, `upload_limit`, `placement_fee`, `fixed_fee`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 10, 'test', 'test test', 'sdgsdg', 1, 1, 1, 'test', '$40,000-$50,000', 1, 40, NULL, 0, NULL, '2019-01-16 04:47:25', '2019-01-16 05:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE `job_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'cate 1', 1, NULL, '2019-01-09 07:49:05', '2019-01-09 07:49:05'),
(2, 'cate 2', 1, NULL, '2019-01-09 07:50:42', '2019-01-09 07:50:42'),
(3, 'cat 3', 1, '2019-01-09 07:50:55', '2019-01-09 07:50:51', '2019-01-09 07:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `job_experience`
--

CREATE TABLE `job_experience` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_experience`
--

INSERT INTO `job_experience` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1-Year', 1, NULL, '2019-01-09 07:57:56', '2019-01-09 07:57:56'),
(2, 'fresher', 1, NULL, '2019-01-09 07:58:09', '2019-01-09 07:58:09'),
(3, '2-Year', 1, '2019-01-09 07:58:29', '2019-01-09 07:58:19', '2019-01-09 07:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `job_users`
--

CREATE TABLE `job_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_193651_create_roles_permissions_tables', 1),
(4, '2018_02_27_094920_add_field_to_users', 1),
(6, '2018_03_14_132708_create_settings_table', 2),
(15, '2019_01_04_071258_create_industry_specializations_table', 3),
(16, '2019_01_04_071502_create_country_table', 3),
(17, '2019_01_04_071518_create_state_table', 3),
(18, '2019_01_04_071620_create_city_table', 3),
(19, '2019_01_04_071703_create_employee_type_table', 3),
(20, '2019_01_04_071739_create_pages_table', 3),
(21, '2019_01_04_071037_create_skills_table', 4),
(23, '2019_01_08_093253_create_company_table', 5),
(24, '2019_01_09_123340_create_job_category', 6),
(25, '2019_01_09_123532_create_job_experience', 6),
(27, '2019_01_10_104325_add_status_to_users_table', 8),
(28, '2019_01_10_112852_create_skills_job_table', 9),
(29, '2019_01_10_112918_create_user_job_table', 9),
(30, '2019_01_09_133409_create_job_table', 10),
(32, '2019_01_16_133951_create_employer_agents_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('krishna.citrusbug@gmail.com', '$2y$10$CGYIFtcqfMWfveFul0fRaOgwi3PdlSsyy.cQ2z.21n/bkkRHWfwMm', '2019-01-10 04:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `parent_id`, `name`, `label`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'access.users', 'Can Access Users', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(2, 1, 'access.user.create', 'Can Create User', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(3, 1, 'access.user.edit', 'Can Edit User', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(4, 1, 'access.user.delete', 'Can Delete User', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(5, 0, 'access.roles', 'Can Access Roles', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(6, 5, 'access.role.create', 'Can Create Role', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(7, 5, 'access.role.edit', 'Can Edit Role', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(8, 5, 'access.role.delete', 'Can Delete Role', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(9, 0, 'access.permissions', 'Can Access Permission', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(10, 9, 'access.permission.create', 'Can Create Permission', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(11, 9, 'access.permission.edit', 'Can Edit Permission', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(12, 9, 'access.permission.delete', 'Can Delete Permission', NULL, '2019-01-02 23:50:33', '2019-01-02 23:50:33'),
(13, 0, 'access.settings', 'Can Access Settings', NULL, NULL, NULL),
(14, 13, 'access.setting.edit', 'Can Edit Settings', NULL, NULL, NULL),
(15, 13, 'access.setting.delete', 'Can Delete Settings', NULL, NULL, NULL),
(16, 0, 'access.skills', 'Can Access Skills', NULL, '2019-01-04 03:03:11', '2019-01-04 03:03:11'),
(17, 16, 'access.skills.create', 'Can Create Skills', NULL, '2019-01-04 03:03:34', '2019-01-04 03:03:34'),
(18, 16, 'access.skills.edit', 'Can Edit Skills', NULL, '2019-01-04 03:03:55', '2019-01-04 03:03:55'),
(19, 16, 'access.skills.delete', 'Can Delete Skills', NULL, '2019-01-04 03:04:15', '2019-01-04 03:04:15'),
(20, 0, 'access.specializations', 'Can Access Industry Specializations', NULL, '2019-01-04 03:05:20', '2019-01-04 03:05:20'),
(21, 20, 'access.specializations.create', 'Can Create Industry Specializations', NULL, '2019-01-04 03:06:01', '2019-01-04 03:06:01'),
(22, 20, 'access.specializations.edit', 'Can Edit Industry Specializations', NULL, '2019-01-04 03:06:29', '2019-01-04 03:06:29'),
(23, 20, 'access.specializations.delete', 'Can Delete Industry Specializations', NULL, '2019-01-04 03:06:52', '2019-01-04 03:06:52'),
(24, 0, 'access.pages', 'Can Access Pages', NULL, '2019-01-04 03:07:47', '2019-01-04 03:07:47'),
(25, 24, 'access.pages.edit', 'Can Edit Pages', NULL, '2019-01-04 03:08:26', '2019-01-04 03:08:26'),
(26, 24, 'access.pages.create', 'Can Create Pages', NULL, '2019-01-04 03:08:44', '2019-01-04 03:08:44'),
(27, 24, 'access.pages.delete', 'Can Delete Pages', NULL, '2019-01-04 03:12:17', '2019-01-04 03:12:17'),
(28, 0, 'access.employeetypes', 'Can Access Employee Type', NULL, '2019-01-04 03:13:01', '2019-01-04 03:13:01'),
(29, 28, 'access.employeetypes.create', 'Can Create Employee Type', NULL, '2019-01-04 03:13:27', '2019-01-04 03:13:27'),
(30, 28, 'access.employeetypes.edit', 'Can Edit Employee Type', NULL, '2019-01-04 03:13:45', '2019-01-04 03:13:45'),
(31, 28, 'access.employeetypes.delete', 'Can Delete Employee Type', NULL, '2019-01-04 03:14:08', '2019-01-04 03:14:08'),
(32, 0, 'access.jobcategory', 'can Job category', NULL, '2019-01-09 07:16:37', '2019-01-09 07:16:37'),
(33, 32, 'access.jobcategory.create', 'Can Create JobCategory', NULL, '2019-01-09 07:17:44', '2019-01-09 07:17:44'),
(34, 32, 'access.jobcategory.edit', 'Can Edit JobCategory', NULL, '2019-01-09 07:18:47', '2019-01-09 07:19:09'),
(35, 32, 'access.jobcategory.delete', 'Can Delete JobCategory', NULL, '2019-01-09 07:20:09', '2019-01-09 07:20:09'),
(36, 0, 'access.jobexperience', 'Can Job Experience', NULL, '2019-01-09 07:22:18', '2019-01-09 07:32:20'),
(37, 36, 'access.jobexperience.create', 'Can Create Job Experience', NULL, '2019-01-09 07:23:28', '2019-01-09 07:23:28'),
(38, 36, 'access.jobexperience.edit', 'Can Edit Job Expericence', NULL, '2019-01-09 07:24:33', '2019-01-09 07:24:33'),
(39, 36, 'access.jobexperience.delete', 'Can Delete Job Experience', NULL, '2019-01-09 07:25:49', '2019-01-09 07:25:49'),
(40, 0, 'access.job', 'Can Access Job', NULL, '2019-01-10 06:01:06', '2019-01-10 06:01:06'),
(41, 40, 'access.job.create', 'Can Job Create', NULL, '2019-01-10 06:02:08', '2019-01-10 06:02:08'),
(42, 40, 'access.job.edit', 'Can Job Edit', NULL, '2019-01-10 06:02:33', '2019-01-10 06:02:33'),
(43, 40, 'access.job.delete', 'Can Job Delete', NULL, '2019-01-10 06:03:08', '2019-01-10 06:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 4),
(2, 1),
(2, 4),
(3, 1),
(3, 4),
(4, 1),
(4, 4),
(5, 1),
(5, 4),
(6, 1),
(6, 4),
(7, 1),
(7, 4),
(8, 1),
(8, 4),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(13, 3),
(13, 4),
(14, 1),
(14, 3),
(14, 4),
(15, 1),
(15, 3),
(15, 4),
(16, 1),
(16, 4),
(17, 1),
(17, 4),
(18, 1),
(18, 4),
(19, 1),
(19, 4),
(20, 1),
(20, 4),
(21, 1),
(21, 4),
(22, 1),
(22, 4),
(23, 1),
(23, 4),
(24, 1),
(24, 4),
(25, 1),
(25, 4),
(26, 1),
(26, 4),
(27, 1),
(27, 4),
(28, 1),
(28, 4),
(29, 1),
(29, 4),
(30, 1),
(30, 4),
(31, 1),
(31, 4),
(32, 1),
(32, 4),
(33, 1),
(33, 4),
(34, 1),
(34, 4),
(35, 1),
(35, 4),
(36, 1),
(36, 4),
(37, 1),
(37, 4),
(38, 1),
(38, 4),
(39, 1),
(39, 4),
(40, 1),
(40, 2),
(40, 4),
(41, 1),
(41, 2),
(41, 4),
(42, 1),
(42, 2),
(42, 4),
(43, 1),
(43, 2),
(43, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SU', 'Super Admin', NULL, '2019-01-02 23:50:34', '2019-01-02 23:50:34'),
(2, 'EMP', 'Employer', NULL, '2019-01-03 04:43:19', '2019-01-04 04:27:41'),
(3, 'AG', 'Agent', NULL, '2019-01-08 02:29:36', '2019-01-08 02:29:36'),
(4, 'AD', 'admin', NULL, '2019-01-15 23:43:38', '2019-01-15 23:43:38');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 10),
(3, 12),
(4, 11);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `value`) VALUES
(1, NULL, NULL, NULL, 'Company_Name', 'HR PORTAL');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `name`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'PHP', NULL, 1, NULL, '2019-01-04 04:04:31', '2019-01-10 05:45:33'),
(2, 'MySQL', NULL, 1, NULL, '2019-01-10 05:33:38', '2019-01-10 05:45:24'),
(3, 'Angular JS', NULL, 1, NULL, '2019-01-10 05:34:23', '2019-01-10 05:42:28'),
(4, 'Js', NULL, 1, NULL, '2019-01-10 05:34:42', '2019-01-10 05:40:33'),
(5, 'a', NULL, 1, NULL, '2019-01-10 05:45:50', '2019-01-10 05:49:45'),
(6, 'az', NULL, 1, NULL, '2019-01-10 05:46:01', '2019-01-10 05:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `skills_job`
--

CREATE TABLE `skills_job` (
  `id` int(10) UNSIGNED NOT NULL,
  `skills_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills_job`
--

INSERT INTO `skills_job` (`id`, `skills_id`, `job_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 1, 1, NULL, NULL, NULL),
(8, 2, 1, NULL, NULL, NULL),
(9, 3, 1, NULL, NULL, NULL),
(10, 4, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`id`, `name`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Specialization10', NULL, 0, '2019-01-10 05:40:20', '2019-01-07 04:46:15', '2019-01-10 05:40:20'),
(2, 'Specialization 2-', NULL, 0, '2019-01-10 05:36:47', '2019-01-10 05:36:42', '2019-01-10 05:36:47'),
(3, 'sss', NULL, 0, '2019-01-10 05:37:24', '2019-01-10 05:37:22', '2019-01-10 05:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `country_id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gujarat', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `profile_image`, `gender`, `contact`, `email`, `password`, `deleted_at`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Admin@admin', '15471130675c37126b45a27.jpg', 'male', '9016415246', 'admin.citrusbug@gmail.com', '$2y$10$YbU5M3skXxSL/lWLUfPgCeskT/fL8G1BQ1wwR3GGVf6MzK3nQPSyi', NULL, 'jxmdpxUfG4k8Wx9pgCFfB2XzrbUjqr02y2XB6SAxw4kWvUsFhubFv7jgcx40', '2019-01-02 23:50:34', '2019-01-10 04:07:47', 1),
(10, 'Krishna', '15472174825c38aa4a1f7dc.jpg', 'female', '9016415246', 'krishna.citrusbug@gmail.com', '$2y$10$BP/HmGqsZgLLGp4JTAn4qesgUQbQgqhHWUFSh0h6eARGOu6zn5UDW', NULL, 'pPyujQn78Iee8iyphWLszvGJRmgOG1nVyzteiz3BfoBvoacji3qhLThkbMsB', '2019-01-10 04:59:37', '2019-01-16 01:27:02', 1),
(11, 'Krishna', NULL, 'female', '9016415246', 'krishna.citrusbug+7@gmail.com', '$2y$10$Ycg2lL/DX1YRsEbqqrA5p.qtlg2RUPcf1jq9WeIC.pBqpU.NjboeS', NULL, '2PYUmaH52Ztgji2Ge79wPQduu9csOcguZaxCCO9Nc3Qgm4mjcIbokDq4B2wC', '2019-01-15 23:46:45', '2019-01-16 00:09:17', 1),
(12, 'Krishna', '15476174045c3ec47ce7f62.jpg', 'female', '9016415246', 'krishna.citrusbug+1@gmail.com', '$2y$10$leQ1iT0R13DriAWqxHQY0e7cajZto2s45ScN1xtUg.AysiZTeQ4eG', NULL, 'qxygbvDlIBUHOmCKMQENyXAUUAmRlS91g3J1fTWsXwy5VG8V0ZUkzcDvfHw5', '2019-01-16 00:07:12', '2019-01-16 00:13:24', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employeetypes`
--
ALTER TABLE `employeetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer_agents`
--
ALTER TABLE `employer_agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_category`
--
ALTER TABLE `job_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_category_name_unique` (`name`);

--
-- Indexes for table `job_experience`
--
ALTER TABLE `job_experience`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_experience_name_unique` (`name`);

--
-- Indexes for table `job_users`
--
ALTER TABLE `job_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `skills_name_unique` (`name`);

--
-- Indexes for table `skills_job`
--
ALTER TABLE `skills_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employeetypes`
--
ALTER TABLE `employeetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employer_agents`
--
ALTER TABLE `employer_agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_category`
--
ALTER TABLE `job_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_experience`
--
ALTER TABLE `job_experience`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_users`
--
ALTER TABLE `job_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `skills_job`
--
ALTER TABLE `skills_job`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

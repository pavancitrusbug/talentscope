<?php
function getNextJobCode()
{
    // Get the last created order
    $lastjob =App\Job::orderBy('created_at', 'desc')->first();

    if ($lastjob != null)
        $number = $lastjob->id;
    else
        $number =0;

    return '#TS' . sprintf('%04d', intval($number) + 1);
}
?>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppliedJob extends Model
{
    protected $table = 'applied_jobs';
    protected $fillable = [
         'job_id','user_id','candidate_name' ,'candidate_education','experiance_id','skills','resume'
    ];
    public function skills()
    {
      return $this->belongsToMany(Skills::class,'candidate_skills','resume_id','skill_id');
    }
}

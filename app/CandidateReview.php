<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CandidateReview extends Model
{
    protected $table = 'candidate_reviews';
    protected $fillable = [
         'candidate_id','job_id' ,'agent_id','employer_id','ratting','review','status'
    ];
}

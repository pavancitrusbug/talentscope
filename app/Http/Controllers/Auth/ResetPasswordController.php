<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Notifications\UserActiveNotification;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function resetPassword($user, $password)
    {

        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        $this->guard()->login($user);

        if(empty($user->remember_token)){

            $admin =  \App\User::SelectRaw('users.id')->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->whereIn('roles.name',['AD','SU'])
                  ->where('users.status',1)
                  ->GroupBy('users.id')
                  ->get()
                  ->pluck('id')
                  ->toArray();
            $admins = \App\User::whereIn('id',$admin)->get();
            \Notification::send($admins, New UserActiveNotification($user));

        }

    }
    protected function sendResetResponse($response)
    {

        return redirect($this->redirectPath())
                            ->with('status', trans($response));
    }
}

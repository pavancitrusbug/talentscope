<?php

namespace App\Http\Controllers\Front_End\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use Auth;

class DashboardController extends Controller
{
    //
    public function index(Request $request)
    {
        $jobs = Job::selectRaw('jobs.*,count(applied_jobs.id) as total_applied')
                    ->leftjoin('applied_jobs','applied_jobs.job_id','jobs.id')
                    ->with('user','job_category','job_experience','employee_type')
                    ->where('jobs.status',0)
                    ->where('jobs.user_id',\Auth::user()->id)
                    ->groupBY('jobs.id')
                    ->orderBy('id', 'DESC')
                    ->paginate(4);

        $connection_request = \App\EmployerAgents::selectRaw('company.*,company.id as company_id,employer_agents.friend_status,employer_agents.agents_id,employer_agents.id as request_id')
                    ->leftjoin('users','users.id','employer_agents.agents_id')
                    ->leftjoin('company','company.user_id','employer_agents.agents_id')
                    ->where('employer_agents.friend_status','pending')
                    ->where('employer_agents.employers_id',\Auth::user()->id)
                    ->orderBy('employer_agents.id')
                    ->first();


        return view('front-end.employee.dashboard',compact('jobs','connection_request'));
    }
}


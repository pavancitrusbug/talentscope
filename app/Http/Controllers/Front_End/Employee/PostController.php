<?php

namespace App\Http\Controllers\Front_End\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use Auth;
use App\JobCategory;
use App\JobExperience;
use App\Employeetypes;
use App\Notifications\NewJobNotification;
use App\Skills;
use Session;

class PostController extends Controller
{
    //

    public function index(Request $request)
    {
        $employeetypes = Employeetypes::where('status',1)->get();
        $employeetypes = $employeetypes->pluck('name', 'id')->prepend('--Select employee type--','');

        $jobexperience = JobExperience::where('status',1)->get();
        $jobexperience = $jobexperience->pluck('name', 'id')->prepend('--Select job experience--','');

        $jobcategory = JobCategory::where('status',1)->get();
        $jobcategory = $jobcategory->pluck('name', 'id')->prepend('--Select job category--','');

        $skills=Skills::where('status',1)->get();


        return view('front-end.employee.post_job',compact('employeetypes','jobexperience','jobcategory','skills'));

    }

    public function post_job(Request $request)
    {

        $this->validate($request,
        [
        'title' => 'required',
        'description' => 'required', //unique:users,email,NULL,id,deleted_at,NULL
        'perks&benifits' => 'required',
        'skills' => 'required',
        'job_category_id' => 'required',
        'employement_type_id' => 'required',
        'job_experience_id' => 'required',
        'location' => 'required',
        'upload_limit' => 'required|numeric',
        'salary' => 'required',
        'expiry_date' => 'required|date',
        ]
    );
        $data = request()->except(['_token','skills','job_id']);
        //dd($data);
        $data['user_id']=Auth::user()->id;

        if(isset($request->job_id))
        {
            $data = request()->except(['_token','skills','job_id']);

            $job=Job::where('id',$request->job_id)->update($data);
            $job=Job::find($request->job_id);
            if($job->skills()->count() > 0)
            {
                $job->skills()->detach();
            }

            $job->skills()->attach($request->skills);
            Session::flash('flash_message', __('Job Updated!'));
        }
        else
        {
            $data['job_code'] = getNextJobCode();
            $job = Job::create($data);
            $job->skills()->attach($request->skills);
            Session::flash('flash_message', __('Job added!'));

            $agents =  \App\EmployerAgents::where('employers_id',Auth::user()->id)->where('friend_status','approve')->get()->pluck('agents_id')->toArray();

            $users = \App\User::whereIn('id',$agents)->get();
            \Notification::send($users, New NewJobNotification($job));
        }

        return redirect('/employess/jobs');
    }

    public function edit(Request $request)
    {
        $job = Job::find($request->id);

        if($job == null)
        {
            Session::flash('flash_message', __('Job added!'),'error');
            return redirect('/employess');
        }
        else
        {
            $employeetypes = Employeetypes::where('status',1)->get();
            $employeetypes = $employeetypes->pluck('name', 'id')->prepend('--Select employee type--','');

            $jobexperience = JobExperience::where('status',1)->get();
            $jobexperience = $jobexperience->pluck('name', 'id')->prepend('--Select job experience--','');

            $jobcategory = JobCategory::where('status',1)->get();
            $jobcategory = $jobcategory->pluck('name', 'id')->prepend('--Select job category--','');

            $skills = Skills::where('status',1)->get();

            $skills_data=$job->skills()->get()->pluck('id')->toArray();

            return view('front-end.employee.post_job',compact('employeetypes','jobexperience','jobcategory','skills','job','skills_data'));

        }
    }

    public function manage_jobs(Request $request)
    {
        $jobs = Job::with('user','job_category','job_experience','employee_type')
                    ->where('status',0)
                    ->where('jobs.user_id',\Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->paginate(4);
        return view('front-end.employee.manage_jobs',compact('jobs'));
    }
}

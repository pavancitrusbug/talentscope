<?php

namespace App\Http\Controllers\Front_End\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\RequestStatusChangeNotification;

class AgentsController extends Controller
{
    public function ChangeStatus($request_id,$agent_id,$status)
    {

        $request = \App\EmployerAgents::find($request_id);
        if($request){
            $request->friend_status = $status;
            $request->save();

            $users = \App\User::find($agent_id);
            \Notification::send($users, New RequestStatusChangeNotification($request));
            return true;
        }
        return false;
    }

    public function AjaxRequestStatusChange(Request $request)
    {
        $rules = array(
            'request_id' => 'required',
            'agent_id' => 'required',
            'status' => 'required',
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json(['success' => false]);
        }
        if($this->ChangeStatus($request->request_id,$request->agent_id,$request->status)){
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }
}

<?php

namespace App\Http\Controllers\Front_End\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CandidateHiredNotification;

class CandidateController extends Controller
{

    public function jobCandidates($job_id)
    {
        $job = \App\Job::with('user','job_category','job_experience','employee_type')->find($job_id);
        $resumes = \App\AppliedJob::selectRaw('applied_jobs.*,job_experience.name as job_experience,company.name as company_name,company.id as company_id')
                                    ->leftjoin('job_experience','job_experience.id','applied_jobs.experiance_id')
                                    ->leftjoin('company','company.user_id','applied_jobs.user_id')
                                    ->where('applied_jobs.job_id',$job_id)
                                    ->where('applied_jobs.status',1)
                                    ->latest()
                                    ->get();
        auth()->user()->unreadNotifications()->where('type','App\Notifications\AppliedforJobNotification')->update(['read_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        return view('front-end.employee.view_candidate',compact('job','resumes'));

    }

    public function Candidatehired(Request $request)
    {

        $candidate_id = $request->candidate_id;

        $applied_candidate = \App\AppliedJob::find($candidate_id);
        if($applied_candidate){
            $applied_candidate->hired_status = 'hired';
            $applied_candidate->save();
            $review_data = array(
                'candidate_id' => $applied_candidate->id,
                'job_id' => $applied_candidate->job_id,
                'agent_id' => $applied_candidate->user_id,
                'employer_id' => \Auth::user()->id,
                'ratting' => $request->stars,
                'review' => $request->review,
            );
            $users = \App\User::find($applied_candidate->user_id);
            \Notification::send($users, New CandidateHiredNotification($applied_candidate));
            \App\CandidateReview::create($review_data);
            \Session::flash('flash_message', 'Candidate Hired Successfully');
            return redirect()->back();
        }
        \Session::flash('flash_error_message', 'Candidate not found');
        return redirect()->back();
    }

    public function getCandidate($candidate_id)
    {
        $applied_candidate = \App\AppliedJob::find($candidate_id);
        return (['success'=>$applied_candidate]);
    }

}

<?php

namespace App\Http\Controllers\Front_End\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\User;
use Auth;
use App\EmployerAgents;

class DashboardController extends Controller
{
    //
    public function index(Request $request)
    {
        $jobs = array();
        if($jobs = app()->call('App\Http\Controllers\Front_End\Agent\JobController@getJobs')){
            $jobs = $jobs->take(5);
        }

        $reviews = $this->getReviews(\Auth::user()->id);
        /*
            $total_star = $reviews->sum('ratting');
            $total_ratting = $reviews->count();
            $review['ratting'] = round($total_star/$total_ratting,1);
            $review['total_ratting'] = $total_ratting;
        */


        return view('front-end.agent.dashboard',compact('jobs','reviews'));
    }
    public function list_page(Request $request)
    {
        $connected_employer = \App\EmployerAgents::all()->where('friend_status','approve')->where('agents_id',\Auth::user()->id)->pluck('employers_id')->toArray();
        $jobs = array();
        if(!empty($connected_employer)){
            $jobs=Job::whereIn('user_id',$connected_employer)->get();
        }



        $employees=User::leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->leftjoin('company','company.user_id','users.id')
                  ->where('roles.name','EMP')
                  ->where('users.status',1)->get();

        $agents=User::leftjoin('role_user','role_user.user_id','users.id')
                ->leftjoin('roles','role_user.role_id','roles.id')
                ->leftjoin('company','company.user_id','users.id')
                ->where('users.id','!=',Auth::user()->id)
                ->where('roles.name','=','AG')
                ->where('users.status',1)->get();

        return view('front-end.agent.list_page',compact('jobs','employees','agents'));
    }

    public function search_data(Request $request)
    {
        $keyword="%".$request->search_data."%";
        
        $jobs=Job::where('title','LIKE',$keyword)->get();
        
        $employees=User::leftjoin('role_user','role_user.user_id','users.id')
                    ->leftjoin('roles','role_user.role_id','roles.id')
                    ->leftjoin('company','company.user_id','users.id')
                    ->where('users.name','LIKE', $keyword)
                    ->where('roles.name','EMP')
                    ->where('users.status',1)->get();

        $agents=User::leftjoin('role_user','role_user.user_id','users.id')
                ->leftjoin('roles','role_user.role_id','roles.id')
                ->leftjoin('company','company.user_id','users.id')
                ->where('users.id','!=',Auth::user()->id)
                ->where('users.name','LIKE', $keyword)
                ->where('roles.name','=','AG')
                ->where('users.status',1)->get();

        if ($request->ajax()) {
            
            $html=view('front-end.agent.list_view',compact('jobs','employees','agents'))->render();
            return response()->json(['success' => true, 'html' => $html]);
        }

        return view('front-end.agent.list_page',compact('jobs','employees','agents'));
        
    }

    public function send_request(Request $request)
    {
        $employer_agent=new EmployerAgents();
        $employer_agent->employers_id=$request->employer_id;
        $employer_agent->employers_id=$request->employer_id;
        $employer_agent->employers_id=$request->employer_id;
        $employer_agent->save();

    }

    public function getReviews($agent_id)
    {
       return $reviews = \App\CandidateReview::where('agent_id',$agent_id)->get();
    }
}

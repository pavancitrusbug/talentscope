<?php

namespace App\Http\Controllers\Front_End\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployerController extends Controller
{
    public function AllEmpoyer()
    {
        $employees= \App\User::selectRaw('company.*,users.profile_image,users.id as user_id')
                    ->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->leftjoin('company','company.user_id','users.id')
                  ->where('roles.name','EMP')
                  ->where('users.status',1)->get();
        $connected_employer = \App\EmployerAgents::all()->where('agents_id',\Auth::user()->id)->pluck('friend_status','employers_id')->toArray();

        return view('front-end.agent.employer.employer', compact('employees','connected_employer'));
    }

    public function SearchData(Request $request)
    {
        $keyword=$request->title;
        /*
        $employees= \App\User::selectRaw('company.*,users.profile_image,users.id as user_id,employer_agents.friend_status')
                    ->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->leftjoin('company','company.user_id','users.id')
                  ->leftjoin('employer_agents','employer_agents.employers_id','user.id')
                  ->where('users.name','LIKE', "%$keyword%")
                  ->where('roles.name','EMP')

                  ->where('users.status',1)->get(); */
        $employees= \App\User::selectRaw('company.*,users.profile_image,users.id as user_id')
                    ->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->leftjoin('company','company.user_id','users.id')
                  ->where('roles.name','EMP')
                  ->where('users.name','LIKE', "%$keyword%")
                  ->where('users.status',1)->get();

        $connected_employer = \App\EmployerAgents::all()->where('agents_id',\Auth::user()->id)->pluck('friend_status','employers_id')->toArray();

        $html=view('front-end.agent.employer.view_employer',compact('employees','connected_employer'))->render();
        return response()->json(['success' => true, 'html' => $html]);
    }

    public function SendRequest(Request $request)
    {
        $employer_agent=new \App\EmployerAgents();
        $employer_agent->employers_id=$request->employer_id;
        $employer_agent->agents_id=\Auth::user()->id;
        $employer_agent->friend_status='pending';
        $employer_agent->save();
        $users = \App\User::find($request->employer_id);
        \Notification::send($users, New \App\Notifications\RequestNotification($employer_agent));

        $employees= \App\User::selectRaw('company.*,users.profile_image,users.id as user_id,employer_agents.friend_status')
                    ->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->leftjoin('company','company.user_id','users.id')
                  ->leftjoin('employer_agents','employer_agents.employers_id','company.id')
                  ->where('roles.name','EMP')
                  ->where('users.status',1)->get();

        $html=view('front-end.agent.employer.view_employer',compact('employees'))->render();

        return response()->json(['success' => true, 'html' => $html]);
    }
}

<?php

namespace App\Http\Controllers\Front_End\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\AppliedforJobNotification;

class JobController extends Controller
{

    public function getJobResume($job_id)
    {
        $resumes = \App\AppliedJob::selectRaw('applied_jobs.*,job_experience.name as job_experience')
                                    ->leftjoin('job_experience','job_experience.id','applied_jobs.experiance_id')
                                    ->where('applied_jobs.job_id',$job_id)
                                    ->where('applied_jobs.user_id',\Auth::user()->id)
                                    ->get();
        $job =  \App\Job::find($job_id);
       // dump($resumes); exit;
        return view('front-end.agent.uploadedresumes',compact('resumes','job'));

    }
    public function UploadResume($job_id = '')
    {
        if(!empty($job_id)){
            $jobs = \App\AppliedJob::selectRaw('count(id) as total_resume')->where('user_id',\Auth::user()->id)->where('job_id',$job_id)->first();
            $job = \App\Job::find($job_id);
            if($jobs->total_resume >= $job->upload_limit ){
                return redirect(url('agent'))->withErrors('You are already uploaded resumes');
            }
        }
        $skills =  \App\Skills::where('status',1)->get()->pluck('name','id')->toArray();
        $jobs = $this->getJobs();
        $jobs =  $jobs->pluck('title','id')->toArray();

        $experiance =  \App\JobExperience::where('status',1)->get()->pluck('name','id')->toArray();

        return view('front-end.agent.uploadresume',compact('skills','jobs','experiance','job_id'));

    }
    public function StoreResume(Request $request)
    {

        $this->validate($request,[
            'jobs' => 'required',
            'skills' => 'required',
            'candidate_name' => 'required',
            'candidate_education' => 'required',
            'candidate_experiance'=>'required',
            'resume'=>'mimes:pdf,doc,docx'
        ]);
        $resume_file_name = null;
        if ($request->hasFile('resume')) {
            $file_name = $request->candidate_name;
            $resume_file_name = $this->uploadFile($request,$file_name);
        }
        $jobs = \App\AppliedJob::selectRaw('count(id) as total_resume')->where('user_id',\Auth::user()->id)->where('job_id',$request->jobs)->first();
            $job = \App\Job::find($request->jobs);
            if($jobs->total_resume >= $job->upload_limit ){
                return redirect()->back()->withErrors('You are already uploaded resumes');
            }
        $jobresumes = new \App\AppliedJob;
        $jobresumes->job_id = $request->jobs;
        $jobresumes->user_id= \Auth::user()->id;
        $jobresumes->skills =serialize( $request->skills);
        $jobresumes->candidate_name =$request->candidate_name;
        $jobresumes->candidate_education =$request->candidate_education;
        $jobresumes->experiance_id =$request->candidate_experiance;
        $jobresumes->resume=$resume_file_name;
        $jobresumes->save();
        $jobresumes->skills()->attach($request->skills);
        $users = \App\User::find($job->user_id);
        \Notification::send($users, New AppliedforJobNotification($jobresumes));

        return redirect (url('agent/agency-upload-resume'))->with('message', 'Resume Posted Successfully');;
    }

    public function uploadFile(Request $request,$file_name)
    {
        $name = null;
        // $current_time =date('d-m-Y');
       // $current_time = Carbon::now()->toDateTimeString();
        if ($request->hasFile('resume')) {
            $file = $request->file('resume');
            $timestamp = $file_name."_JOBS_".$request->jobs;
            $name = $timestamp.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/resumes/',$name);
		}
		return $name;
    }
    public function editResume($resume_id)
    {
        $skills =  \App\Skills::where('status',1)->get()->pluck('name','id')->toArray();
        $jobs =  \App\Job::get()->pluck('title','id')->toArray();
        $experiance =  \App\JobExperience::where('status',1)->get()->pluck('name','id')->toArray();
        $resume = \App\AppliedJob::find($resume_id);
        return view('front-end.agent.editresume',compact('skills','jobs','experiance','resume'));
    }

     public function updateResume(Request $request,$resume_id)
    {

        $this->validate($request,[
            'jobs' => 'required',
            'skills' => 'required',
            'candidate_name' => 'required',
            'candidate_education' => 'required',
            'candidate_experiance'=>'required',
            'resume'=>'mimes:pdf,doc,docx'
        ]);
        $jobresumes =  \App\AppliedJob::find($resume_id);
        $resume_file_name = null;
        if ($request->hasFile('resume')) {
            $file_name = $request->candidate_name;
            $resume_file_name = $this->uploadFile($request,$file_name);
                if($jobresumes->resume){
                if(file_exists(public_path(). '/uploads/resumes/'.$jobresumes->resume) AND !empty($jobresumes->resume)){
                    unlink(public_path(). '/uploads/resumes/'.$jobresumes->resume);
                }
            }
        }


        $jobresumes->job_id = $request->jobs;
        $jobresumes->user_id= \Auth::user()->id;
        $jobresumes->skills =serialize( $request->skills);
        $jobresumes->candidate_name =$request->candidate_name;
        $jobresumes->candidate_education =$request->candidate_education;
        $jobresumes->experiance_id =$request->candidate_experiance;
        $jobresumes->resume=$resume_file_name;
        $jobresumes->save();
        if($jobresumes->skills()->count() > 0)
        {
            $jobresumes->skills()->detach();
        }
        $jobresumes->skills()->attach($request->skills);

        return redirect (url('agent/agency-upload-resume'))->with('Resume Posted updated');;
    }


    public function destroyResume($resume_id)
    {
        $jobresumes =  \App\AppliedJob::find($resume_id);

        if($jobresumes->resume){
            if(file_exists(public_path(). '/uploads/resumes/'.$jobresumes->resume) AND !empty($jobresumes->resume)){
                unlink(public_path(). '/uploads/resumes/'.$jobresumes->resume);
            }
        }
        $jobresumes->delete();
        return redirect()->back()
                        ->with('success','Resume deleted successfully');
    }
    public function getJobs()
    {
        $jobs = array();
        $connected_employer = \App\EmployerAgents::all()->where('friend_status','approve')->where('agents_id',\Auth::user()->id)->pluck('employers_id')->toArray();
        if(!empty($connected_employer)){

            $jobs =  \App\Job::selectRaw('jobs.*,jobs.`perks&benifits` as benifits,count(applied_jobs.id) as total_applied,job_category.name as job_name,employeetypes.name as employee_type_name,job_experience.name as job_experiance')
                            ->leftjoin('applied_jobs','applied_jobs.job_id','jobs.id')
                            ->leftjoin('job_category','job_category.id','jobs.job_category_id')
                            ->leftjoin('employeetypes','employeetypes.id','jobs.employement_type_id')
                            ->leftjoin('job_experience','job_experience.id','jobs.job_experience_id')
                            ->whereIn('jobs.user_id',$connected_employer)
                     //       ->where('jobs.status',1)
                            ->latest()
                            ->groupBy('jobs.id')->get();
        }
        return $jobs;
    }

    public function jobDetails($job_id)
    {
        $jobs = $this->getJobs();
        $job = $jobs->where('id',$job_id)->first();
        if($job){
            $candidates = \App\AppliedJob::where('job_id',$job_id)->where('user_id',\Auth::user()->id)->get();
            return view('front-end.agent.jobdetail',compact('job','candidates'));
        }
        \Session::flash('flash_error_message','Job not found');
        return redirect()->back();

    }

}

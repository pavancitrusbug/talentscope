<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\ActivationMail;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Country;
use App\States;
use App\City;
use App\Company;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\CapsuleOwner;
use App\Opportunity;
use App\UserStates;
use App\Specializations;
use DB;
use App\Notifications\Signup;
use App\Notifications\NewEmployerNotification;
use App\Notifications\UserVerifiedNotification;

class UsersController extends Controller

{

    public function __construct()
    {
        $this->middleware('permission:access.users');
        $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.user.create')->only(['create', 'store']);
        $this->middleware('permission:access.user.delete')->only('destroy');
    }



    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $users = User::with('roles')->where('name', 'LIKE', "%$keyword%")
                    ->orWhere('email', 'LIKE', "%$keyword%")
                    ->orderBy('id', 'DESC')
                    ->paginate($perPage);
        } else {
            $users = User::with('roles')->orderBy('id', 'DESC')->paginate($perPage);
        }

        //$users = User::with('roles')->get();

        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        if(Auth::user()->id != 1)
        {
            $roles = Role::where('id','!=',1)->select('id', 'name', 'label')->get();
        }
        else
        {
            $roles = Role::select('id', 'name', 'label')->get();
        }

        $roles = $roles->pluck('label', 'name')->prepend('--Select Role--','');

        $country = Country::select('id', 'name', 'status')->get();
        $country = $country->pluck('name', 'id')->prepend('--Select Country--','');

        $state = States::select('id', 'name', 'status')->get();
        $state = $state->pluck('name', 'id')->prepend('--Select State--','');

        $city = City::select('id', 'name', 'status')->get();
        $city = $city->pluck('name', 'id')->prepend('--Select City--','');

        $specialization = Specializations::select('id', 'name', 'status')->get();
        $specialization = $specialization->pluck('name', 'id')->prepend('--Select Specialization--','');
        //$category = Category::pluck('name','id')->prepend('Select Category','');
        // $capsule = CapsuleOwner::pluck('name','capsule_id')->prepend('--Select--','');

        // $states = States::select('id', 'state', 'code')->get();
        // $states = $states->pluck('state', 'code');


        return view('admin.users.create', compact('roles','country','state','city','specialization'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
            'name' => 'required',
            'email' => 'unique:users,email,NULL,id,deleted_at,NULL', //unique:users,email,NULL,id,deleted_at,NULL
            'roles' => 'required',
            'company_name' => 'required',
            'company_email_address' => 'required',
            'profile_image'=> 'mimes:jpeg,jpg,png|required|max:10000'
            ]
        );
        
        $role = Role::where('name',$request->roles)->first();

        $user = new User();
        $user->name=$request->name;
        $user->gender=$request->gender;
        $user->contact=$request->contact;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);

        if ($request->file('profile_image')) {
            $fimage = $request->file('profile_image');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/user/'), $filename);
            $user->profile_image= $filename;
        }

        $user->save();
        $user->roles()->attach($role);

        $company=new Company();
        $company->name=$request->company_name;

        if ($request->file('company_logo')) {
            $fimage = $request->file('company_logo');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/company/'), $filename);
            $company->logo = $filename;
        }
        $company->email_address=$request->company_email_address;
        $company->user_id=$user->id;
        if(isset($request->company_address))
        {
            $company->address=$request->company_address;
        }
        if(isset($request->company_country))
        {
            $company->country_id=$request->company_country;
        }
        if(isset($request->company_state))
        {
            $company->state_id=$request->company_state;
        }
        if(isset($request->company_city))
        {
            $company->city_id=$request->company_city;
        }
        if(isset($request->company_pincode))
        {
            $company->pin_code=$request->company_pincode;
        }
        if(isset($request->company_contact))
        {
            $company->contact=$request->company_contact;
        }
        if(isset($request->company_about))
        {
            $company->about=$request->company_about;
        }
        if(isset($request->company_website))
        {
            $company->website=$request->company_website;
        }
        if(isset($request->company_pincode))
        {
            $company->pin_code=$request->company_pincode;
        }
        if(isset($request->company_year_founded))
        {
            $company->year_founded=$request->company_year_founded;
        }
        if(isset($request->company_verified))
        {
            $company->verified=$request->company_verified;
        }
        $company->save();
        if(isset($request->specialization))
        {
            if($company->specialization()->count() > 0)
            {
                $company->specialization()->detach();
            }

            $specialization=$request->specialization;

            for($i=0;$i<=count($specialization);$i++)
            {
                $specialization_data = Specializations::find($specialization[$i]);
                if($specialization_data == null)
                {
                    $specialization_details=new Specializations();
                    $specialization_details->name=$specialization[$i];
                    $specialization_details->save();
                    $company->specialization()->attach($specialization_details->id);

                }
                else
                {
                    $company->specialization()->attach($specialization[$i]);
                }
            }
        }

       

        $token = app('auth.password.broker')->createToken($user);
        $user->notify(new Signup($user, $token));
        if($company->verified == 1){
            \Notification::send($user, New UserVerifiedNotification($company));
        }
        if($role->name == 'EMP'){
            $agents =  \App\User::SelectRaw('users.id')->leftjoin('role_user','role_user.user_id','users.id')
                  ->leftjoin('roles','role_user.role_id','roles.id')
                  ->where('roles.name','AG')
                  ->where('users.status',1)
                  ->GroupBy('users.id')
                  ->get()
                  ->pluck('id')
                  ->toArray();
            $agent = \App\User::whereIn('id',$agents)->get();
            \Notification::send($agent, New NewEmployerNotification($user));

        }

        Session::flash('flash_message', __('User added!'));

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();


        if($user){
            // $opportunities = Opportunity::where('owner_id',$user->capsule_id)->get();
            return view('admin.users.show', compact('user'));
        }else{
            return redirect('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        if($id == 1 && Auth::user()->id != 1){

            return redirect()->back();
        }

        if(Auth::user()->id != 1)
        {
            $roles = Role::where('id','!=',1)->select('id', 'name', 'label')->get();
        }
        else
        {
            $roles = Role::select('id', 'name', 'label')->get();
        }

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name')->prepend('--Select Role--','');

        $country = Country::select('id', 'name', 'status')->get();
        $country = $country->pluck('name', 'id')->prepend('--Select Country--','');

        $state = States::select('id', 'name', 'status')->get();
        $state = $state->pluck('name', 'id')->prepend('--Select State--','');

        $city = City::select('id', 'name', 'status')->get();
        $city = $city->pluck('name', 'id')->prepend('--Select City--','');

        $specialization = Specializations::select('id', 'name', 'status')->get();
        $specialization = $specialization->pluck('name', 'id')->prepend('--Select Specialization--','');

        $user = User::where('id',$id)->first();

        if($user){

            $user_roles = [];
            foreach ($user->roles as $role) {
                $user_roles[] = $role->name;
            }
            $company=Company::where('user_id',$user->id)->first();
            $company_country = [];
            foreach ($user->roles as $role) {
                $user_roles[] = $role->name;
            }

            // $user_states = [];
            // foreach($user->state as $state){
            //     $user_states[] = $state->state_id;
            // }

            // $capsule = CapsuleOwner::pluck('name','capsule_id')->prepend('--Select--','');
            return view('admin.users.edit', compact('user', 'roles','company','specialization','city','state','country','user_roles'));
        }else{
            return redirect('admin/users');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,
            [
            'name' => 'required',
            'roles' => 'required',
            'company_name' => 'required',
            'company_email_address' => 'required',
            'profile_image'=> 'mimes:jpeg,jpg,png|required|max:10000'
            ]
        );

      //dd($request->company_name);
        $user = User::findOrFail($id);
        $user->name=$request->name;
        $user->gender=$request->gender;
        $user->contact=$request->contact;
       // $user->email=$request->email;

        if ($request->file('profile_image')) {
            $fimage = $request->file('profile_image');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/user/'), $filename);
            $data['profile_image'] = $filename;
            $user->profile_image=$data['profile_image'];
        }
        $user->save();

        $company=Company::where('user_id',$user->id)->first();
      
        $previous_company = $company;

        if ($request->file('company_logo')) {
            $fimage = $request->file('company_logo');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/company/'), $filename);
            $company->logo = $filename;
        }
        
        $company->name=$request->company_name;
        $company->email_address=$request->company_email_address;
        $company->user_id=$user->id;
        if(isset($request->company_address))
        {
            $company->address=$request->company_address;
        }
        if(isset($request->company_country))
        {
            $company->country_id=$request->company_country;
        }
        if(isset($request->company_state))
        {
            $company->state_id=$request->company_state;
        }
        if(isset($request->company_city))
        {
            $company->city_id=$request->company_city;
        }
        if(isset($request->company_pincode))
        {
            $company->pin_code=$request->company_pincode;
        }
        if(isset($request->company_contact))
        {
            $company->contact=$request->company_contact;
        }
        if(isset($request->company_about))
        {
            $company->about=$request->company_about;
        }
        if(isset($request->company_website))
        {
            $company->website=$request->company_website;
        }
        if(isset($request->company_pincode))
        {
            $company->pin_code=$request->company_pincode;
        }
        if(isset($request->company_year_founded))
        {
            $company->year_founded=$request->company_year_founded;
        }
        if(isset($request->company_verified))
        {
            $company->verified=$request->company_verified;
        }
        $company->save();
        if(isset($request->specialization))
        {
           
            $specialization=$request->specialization;
            for($i=0;$i<count($specialization);$i++)
            {
                $specialization_data = Specializations::where('id',$specialization[$i])->first();
                if($specialization_data == null)
                {
                    $specialization_details=new Specializations();
                    $specialization_details->name=$specialization[$i];
                    $specialization_details->save();
                    $company->specialization()->attach($specialization_details->id);

                }
                else
                {
                    $company->specialization()->attach($specialization[$i]);
                }
            }
        }
     

        if($previous_company->verified == 0 && $company->verified == 1){
            \Notification::send($user, New UserVerifiedNotification($company));
        }

        $role = Role::where('name',$request->roles)->first();

        if($request->has('roles') && ( $request->roles != null || $request->roles != '' )){
            $user->roles()->detach();
            $user->roles()->attach($role);
        }

        Session::flash('flash_message', __('User updated!'));
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request ,$id)
    {
        $result = array();
        if(Auth::user()->id==$id  || $id==1 ){
            $result['message'] = "You don't have permission to delete this user";
            $result['code'] = 400;
        }else{
           try {
                $res = User::where("id",$id)->first();
                if ($res) {
                    Company::where('user_id',$id)->delete();
                    User::where("id",$id)->delete();

                    $result['message'] = "Record Deleted Successfully.";
                    $result['code'] = 200;
                } else {
                    $result['message'] = "Something went wrong , Please try again later.";
                    $result['code'] = 400;
                }
            } catch (\Exception $e) {
                $result['message'] = $e->getMessage();
                $result['code'] = 400;
            }
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{

            Session::flash('flash_message','User Deleted Successfully!');
            return redirect('admin/users');
        }
    }
    public function update_status(Request $request)
    {
           $user=User::find($request->id);

           if($request->status == 1)
           {
               $user->status=0;
           }
           else{
               $user->status=1;
           }

           $user->save();

        return response()->json(['success' => true]);
    }

}

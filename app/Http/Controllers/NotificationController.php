<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function getnotification()
    {
        $notify = array();
        if(auth()->user()->unreadNotifications->count() > 0){
            foreach (auth()->user()->unreadNotifications as $notification){
                $data = array();
                if (isset($notification->data['applied_job'])){
                    $jobs = \App\Job::selectRaw('jobs.*,applied_jobs.user_id as agent_id,company.name as company_name')
                    ->leftjoin('applied_jobs','applied_jobs.job_id','jobs.id')
                    ->leftjoin('company','company.user_id','applied_jobs.user_id')
                    ->with('user','job_category','job_experience','employee_type')
                    ->where('jobs.status',0)
                    ->where('jobs.id',$notification->data['applied_job']['job_id'])
                    ->first();
                    $user = \App\User::find($jobs->agent_id);
                    $data['user_name'] = $user->name;
                    $data['title'] = $user->name;
                    $data['user_image'] = ($user->profile_image)?$user->profile_image:'userdefault.png';
                    $data['descriptions'] = "$jobs->company_name Submit Resume for $jobs->title";
                    $data['url'] = url("employess/candidates")."/$jobs->id";
                } else if(isset($notification->data['new_user'])){
                    $user = $notification->data['new_user'];
                    $data['user_name'] = $user['name'];
                    $data['title'] = "Welcome To Talentscope";
                    $data['user_image'] = (isset($new_employer['profile_image']))?$new_employer['profile_image']: 'userdefault.png';
                    $data['descriptions'] = "";
                    $data['url'] = '#';
                }
                else if(isset($notification->data['request'])){
                    $users = \App\User::whereIn('id',[$notification->data['request']['employers_id'],$notification->data['request']['agents_id']]);
                    $agents = $users->where('id',$notification->data['request']['agents_id'])->first();
                    $employer = $users->where('id',$notification->data['request']['employers_id'])->first();
                    $data['user_name'] = $agents->name;
                    $data['title'] = "$agents->name send Request.";
                    $data['user_image'] = ($agents->profile_image)?$agents->profile_image:'userdefault.png';
                    $data['descriptions'] = "$agents->name send Request for connection";
                    $data['url'] = '#';
                }
                else if(isset($notification->data['new_job'])){

                    $new_job = $notification->data['new_job'];

                    $users = \App\User::find($new_job['user_id']);
                    $data['user_name'] = $users->name;
                    $data['title'] = "$users->name Added a Job.";
                    $data['user_image'] = ($users->profile_image)?$users->profile_image:'userdefault.png';
                    $data['descriptions'] = "$users->name Added a ".$new_job['title'] . " Job.";
                    $data['url'] = url('agent/job-detail/')."/".$new_job['id'];
                } else if(isset($notification->data['new_employer'])){

                    $new_employer = $notification->data['new_employer'];

                    $data['user_name'] = $new_employer['name'];
                    $data['title'] = "New Employer Registered";
                    $data['user_image'] = (isset($new_employer['profile_image']))?$new_employer['profile_image']: 'userdefault.png';
                    $data['descriptions'] = $new_employer['name']. " Added in Talentscope As a Employer.";
                    $data['url'] = url('agent/employer');
                }
                else if(isset($notification->data['company_verified'])){

                    $company = $notification->data['company_verified'];

                    $data['user_name'] = $company['name'];
                    $data['title'] = "Your Company Verified by Admin.";
                    $data['user_image'] = (!empty(\Auth::user()->profile_image))?\Auth::user()->profile_image: 'userdefault.png';
                    $data['descriptions'] = $company['name']. " verified by the Admin.";
                    $data['url'] = '#';
                }
                else if(isset($notification->data['user_active'])){

                    $user = $notification->data['user_active'];

                    $data['user_name'] = $user['name'];
                    $data['title'] = $user['name']. "is Active now.";
                    $data['user_image'] = (!empty($user['profile_image']))?$user['profile_image']: 'userdefault.png';
                    $data['descriptions'] = $user['name']. " is reset paswword successfully.";
                    $data['url'] = url('admin/users/')."/".$user['id'];
                }
                else if(isset($notification->data['request_status'])){

                    $employer = \App\User::find($notification->data['request_status']['agents_id']);

                    $data['user_name'] = $employer->name;
                    $data['title'] = "$employer->name ".$notification->data['request_status']['friend_status'] ."Your Request";
                    $data['user_image'] = ($employer->profile_image)?$employer->profile_image:'userdefault.png';
                    $data['descriptions'] = "$employer->name ".$notification->data['request_status']['friend_status'] ."Your Request";
                    $data['url'] = url('agent/employer');
                } else if(isset($notification->data['hired_candidate'])){
                    $candidate = $notification->data['hired_candidate'];
                    $job_details = \App\Job::selectRaw('users.*,jobs.*')->leftjoin('users','users.id','jobs.user_id')->where('jobs.id',$candidate['job_id'])->first();


                    $data['user_name'] = $job_details->name;
                    $data['title'] = "$job_details->name hired ".$candidate['candidate_name'];
                    $data['user_image'] = ($job_details->profile_image)?$job_details->profile_image:'userdefault.png';
                    $data['descriptions'] = "$job_details->name ".$candidate['candidate_name'];
                    $data['url'] = url('agent/uploaded-resume/')."/".$candidate['job_id'];
                }

                if(!empty($data)){
                    $notify[] = $data;
                }

            }
        }
        return $notify;
    }

    public function ReadNotification()
    {
        auth()->user()->unreadNotifications->markAsRead();
    }
    public function CountNotification()
    {
        return auth()->user()->unreadNotifications->count();
    }
}

@extends('layouts.frontend')
@section('content')

    <div class="col-lg-12 col-md-12 col-sm-12 clearfix">
        <div class="row">
            <div class="width-100">
                <div class="listing-div2 my-jobs1 job-candidatediv">
                    <div class="heading-div">
                        <h4>View Candidate ({{$job->title}})</h4>
                    </div>
                    <div class="body-listing body-job-details">
                        @if ($resumes)
                            @foreach ($resumes as $resume)
                                <div class="view-candidate-row">

                                    <div class="view-candidate-inner">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="data-row-div2 data-row-employer">
                                                    <h5>{{$resume->candidate_name}}</h5>

                                                    <div class="content-row3">
                                                        <p class="p-blk1">{{$job->title}} | Experience : {{$resume->job_experience}} | {{$job->job_code}}</p>
                                                        <p class="p-blk1">{{$resume->candidate_education}}</p>
                                                        <p>Approved Recruiting Agency: <a href="#" class="link-blue"> {{$resume->company_name}}</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 text-right-1">
                                                <div class="data-row-div2 data-row-employer employer-c2">
                                                    @if ($resume->hired_status == 'pending')
                                                    <div class="dropdown custom-dropdown1">
                                                        <button type="button" class="btn-custom btn-outline1 dropdown-toggle" data-toggle="dropdown">
                                                            Pending Interview
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="#">Link 1</a>
                                                            <a class="dropdown-item" href="#">Link 2</a>
                                                            <a class="dropdown-item" href="#">Link 3</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="btn-custom btn-green approve_candidate" data-resume="{{$resume->id}}" data-action='approve' data-toggle="modal" data-target="#myModal">Approved</a>
                                                    <a href="#" class="btn-custom btn-orange">Interview Confirmed</a>
                                                    <p class="scheduled-time">First interview scheduled at 31st Jan 2019, Citrusbug Technolabs</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($resume->resume)
                                        <div class="pdf-upoad">
                                            <p><img src="{{asset('front-end/images/icons/pdf.png')}}">
                                                <span class="pdf-title">{{$resume->resume}}</span>
                                                <a target="_blank" href="{{url('/uploads/resumes/')}}/{{$resume->resume}}"><button class="btn btn-action2 btn-review2">Review</button></a>
                                            </p>
                                        </div>
                                    @endif


                                    <div class="btn-bottom-div">
                                        @if ($resume->hired_status == 'pending')

                                        <button class="btn btn-primary1 vc-approve approve_candidate" data-resume="{{$resume->id}}" data-action='approve' data-toggle="modal" data-target="#myModal">Approve</button>

                                                <button class="btn btn-primary1 vc-reject" data-resume="{{$resume->id}}" data-action='reject'>Reject</button>

                                        @endif

                                    </div>

                                </div>
                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="model_title">Modal Header</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => url('employess/candidateshired'),'method'=>'POST','id'=>'approve_candidates_form')) }}
                        <input id="ratings-hidden" required="required" name="stars" type="hidden">
                        {{Form::hidden('candidate_id', '',['id'=>'hidden_candidate_id'])}}

                        {{-- <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea> --}}
                        {{Form::textarea('review',null,['class'=>'form-control animated','cols'=>'50','rows'=>5, 'id'=>'new-review','placeholder'=>'Write your Review here'])}}
                        {{-- {{Form::label('stars', 'Ratting')}}
                        {{Form::select('stars', array('5' => '5', '4' => '4','3'=>'3','2'=>'2','1'=>'1'))}} --}}
                        <div class="text-right">
                            <div class="stars starrr" data-rating="0"></div>
                            {{-- <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                             <span class="glyphicon glyphicon-remove"></span>Cancel</a> --}}
                            {{-- <button class="btn btn-success btn-lg" type="submit">Save</button> --}}

                            {{ Form::submit('Approve',['class'=>'btn btn-success btn-lg']) }}
                        </div>

                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerExtra')
<script>

    $(".approve_candidate").on('click',function(e) {
        var action = $(this).attr('data-action');
        var resume_id = $(this).attr('data-resume');


        jQuery.ajax({
            type: 'get',
            url: '{{url("/employess/getcandidatedetails")}}/'+resume_id,
            data:{},
            success: function(response){
                if(response.success)
                {
                    $("#model_title").html(response.success.candidate_name);
                    $("#hidden_candidate_id").val(response.success.id);

                } else {
                    location.reload(true);
                }

            }
        });
    });

    (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='far fa-star'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("far fa-star").addClass("fas fa-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("fas fa-star").addClass("far fa-star")}}if(!e){return this.$el.find("span").removeClass("fas fa-star").addClass("far fa-star")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('.approve_candidate');
  var newReview = $('#new-review');
  var openReviewBtn = $('.approve_candidate');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    // openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  $('#approve_candidates_form').submit(function () {
        var ratting = $.trim($('#ratings-hidden').val());

        if (ratting  === '') {
            alert('Review Star Required');
            return false;
        }
    });
  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.show();

  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});

</script>
@endsection
;
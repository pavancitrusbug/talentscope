@extends('layouts.frontend')

@section('content')
    
    <div class="col-lg-12 col-md-12 col-sm-12 full12-div float-left clearfix">
            <div class="row">
                <div class="width-100">
                  
                        <input type="text" name="search_data" id="search_data"> 
                    
                </div>
            </div>
    </div>
    <div id="list_view">
       @include('front-end.agent.list_view')
    </div>

@endsection

@section('footerExtra')

    <script>
        $(document).ready(function(e){
            $("#search_data").on('keyup',function(e) {
                //if(e.which == 13) {
                   
                    jQuery.ajax({
                        type: 'POST',
                        url: '{{url("/agent/search_data")}}',
                        data:{search_data:$(this).val(),_token:"{{csrf_token()}}"},
                     
                        success: function(response){
                        if(response.success)
                            {
                               $("#list_view").html(response.html);
                            }
                        }
                    });
               //}
            });
            $(".send_request").on('click',function(e) {
                    jQuery.ajax({
                        type: 'POST',
                        url: '{{url("/agent/send_request")}}',
                        data:{employer_id:$(this).attr("data_value"),_token:"{{csrf_token()}}"},
                        success: function(response){
                        if(response.success)
                            {
                               $("#list_view").html(response.html);
                            }
                        }
                    });
            });
        });
    </script>

@endsection

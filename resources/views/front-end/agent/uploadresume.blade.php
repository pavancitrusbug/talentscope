@extends('layouts.frontend')
@section('content')

        <div class="row">
            <div class="width-100">
                <div class="listing-div2 my-jobs1 my-profile">
                    <div class="heading-div">
                        <h4>Upload Resume</h4>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong></strong> {{$errors->first()}}
                        </div>
                    @endif
                    <div class="body-listing body-profile">
                        {{ Form::open(array('method' => 'POST','files'=>'true'))}}
                            <div class="form-div1 clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 form-div6 float-left">
                                    <div class="form-group">
                                        <label>Candidate Full Name <span class="red-decoration">*</span></label>
                                        {{Form::text('candidate_name',null,['class'=>'form-control',"placeholder"=>"Enter candidate full name"])}}
                                        {!! $errors->first('candidate_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-div6 float-left">
                                    <div class="form-group">
                                        <label>For Job Applied <span class="red-decoration">*</span></label>
                                        {!! Form::select('jobs', [''=>'-- Select Job --']+$jobs, $job_id, ['id' => 'jobs', "class"=>"form-control"]) !!}
                                        {!! $errors->first('jobs', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-div6 float-left">
                                    <div class="form-group">
                                        <label>Canddidate Education<span class="red-decoration">*</span></label>
                                        {{Form::text('candidate_education',null,['class'=>'form-control',"placeholder"=>"Enter canddidate education"])}}
                                        {!! $errors->first('candidate_education', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-div6 float-left">
                                    <div class="form-group">
                                        <label>Experiance <span class="red-decoration">*</span></label>
                                        {!! Form::select('candidate_experiance',[''=>'-Select Experiance-']+ $experiance, null, ['id' => 'candidate_experiance', "class"=>"form-control"]) !!}
                                        {!! $errors->first('candidate_experiance', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-div6 float-left">
                                    <div class="form-group">
                                        <label>Skills <span class="red-decoration">*</span></label>
                                        {!! Form::select('skills[]', $skills, [], ['id' => 'skills', "class"=>"form-control", 'multiple' => 'multiple']) !!}
                                        {!! $errors->first('skills', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>


                            </div>

                            <div class="form-div3 clearfix">

                                <div class="col-lg-12 col-md-12 col-sm-12 btn-div01">
                                    <div class="upload-profile-div">
                                        <div class="uploadbtn-div btn-resume">
                                            <div class="file-upload">
                                                {{Form::label('resume', 'Upload Resume',['class'=>'file-upload__label'])}}
                                                {{Form::file('resume', array("id"=>"resume","class"=>"file-upload__input"))}}
                                                {!! $errors->first('resume', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 btn-div01 btn-agency-resume">
                                    {{Form::submit('SUBMIT',['class'=>'btn btn-primary btn-red btn-red-a2 min-width100'])}}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('footerExtra')

<script>
    $(document).ready(function() {
        $('#skills').select2();
        $('#jobs').select2();
        $('#candidate_experiance').select2();


    });
</script>
@endsection
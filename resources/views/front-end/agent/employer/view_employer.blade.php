<div class="col-lg-12 col-md-12 col-sm-12 five-div float-left clearfix">
    <div class="row">
        <div class="request-agency-div">
            <div class="listing-div3">
                <div class="heading-div">
                    <h4>Employees</h4>
                </div>
                <div class="body-listing02">
                    @if(isset($employees))
                        @foreach($employees as $employee)
                            <div class="fullrow-div clearfix">
                                <div class="toprow-div clearfix">
                                    <div class="col-div11">
                                        <div class="agency-thumb1">
                                            @if(isset($employee->profile_image))
                                            <img src="{!! asset('/user/'.$employee->profile_image) !!}" class="img-fluid agency-img1" alt="user-img">                                    @else
                                            <img class="img-fluid agency-img1" src="{!! asset('front-end/images/userdefault.png')!!}" alt="user-img">                                    @endif
                                        </div>
                                        <div class="title-div">
                                            <h5>{{$employee->name}}</h5><a href="#" class="link01">{{$employee->website}}</a></div>
                                    </div>
                                    <div class="col-div1 col-exep">
                                        <div class="title-div1">
                                            <h6>experience :</h6>
                                            <p>{{$employee->year_founded}}</p>
                                        </div>
                                    </div>

                                    <div class="col-div1 col-div3">
                                        <div class="title-div1">
                                            <h6>LOCATION :</h6>
                                            <p><i class="fas fa-map-marker-alt locationi"></i>{{$employee->address}}</p>
                                        </div>
                                    </div>
                                    @if (array_key_exists($employee->user_id,$connected_employer))
                                        <div class="col-div1 col-div4">
                                            <div class="title-div1">
                                                <h6>STATUS :</h6><label class="pending-label">{{$connected_employer[$employee->user_id]}}</label></div>
                                        </div>
                                    @endif
                                </div>
                                <div class="bottomhover-row">
                                    <div class="width-75per">
                                        <a href="#" class="link-blk1">VIEW EMPLOYER PROFILE</a>
                                    </div>
                                    @if (!array_key_exists($employee->user_id,$connected_employer))
                                    <div class="width-25per">
                                        <button class="btn btn-action btn-primary send_request" data_value="{{$employee->user_id}}">
                                            Send Request
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

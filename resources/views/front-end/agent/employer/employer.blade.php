@extends('layouts.frontend')
@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 full12-div float-left clearfix">
    <div class="row">
        <div class="width-100">

            <input type="text" id="search_data" name="search_data" id="search_data">

        </div>
    </div>
</div>
<div id="view_employer">
    @include('front-end.agent.employer.view_employer')
</div>
@endsection

@section('footerExtra')

<script>
    $(document).ready(function(e){
            $("#search_data").on('keypress',function(e) {
                if(e.which == 13) {
                    jQuery.ajax({
                        type: 'POST',
                        url: '{{url("/agent/employer/search_data")}}',
                        data:{title:$(this).val(),_token:"{{csrf_token()}}"},

                        success: function(response){
                        if(response.success)
                            {
                               $("#view_employer").html(response.html);
                            }
                        }
                    });
                }
            });
            $(".send_request").on('click',function(e) {
                    jQuery.ajax({
                        type: 'POST',
                        url: '{{url("/agent/employer/send_request")}}',
                        data:{employer_id:$(this).attr("data_value"),_token:"{{csrf_token()}}"},
                        success: function(response){
                        if(response.success)
                            {
                                alert('Request send Sucessfully');
                               $("#view_employer").html(response.html);
                            }
                        }
                    });
            });
        });
</script>
@endsection
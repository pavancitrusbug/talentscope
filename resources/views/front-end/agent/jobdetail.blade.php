@extends('layouts.frontend')
@section('content')
        <div class="col-lg-12 col-md-12 col-sm-12 clearfix">
            <div class="row">
                <div class="width-100">
                    <div class="listing-div2 my-jobs1 job-details-div">
                        <div class="heading-div">
                            <h4>Job Detail</h4>
                        </div>
                        <div class="body-listing body-job-details">

                            <div class="data-row-div data-job-row1">
                                <h5>{{$job->title}} </h5>
                                <div class="btn-tr-div"><a class="btn btn-action2 btn-review2 " href="{{url("agent/upload-resume")}}/{{$job->id}}">Recruit Talent</a></div>
                                <p class="p-blk1">Experience: {{$job->job_experiance}} | PHP, MySQL, Wordpress</p>
                                <p class="p-blk1">{{$job->location}}</p>
                                <p class="p-blk1">Fee: {{($job->placement_fee)?$job->placement_fee."%":"Fixed price ".$job->fixed_fee}}</p>
                                <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on {{\Carbon\Carbon::parse($job->created_at)->format('dS F Y')}}</p>
                                <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on {{\Carbon\Carbon::parse($job->expired_on)->format('dS F Y')}}</p>


                            </div>

                            <div class="data-job-row2">
                                <h4>Job Description :</h4>
                                <p class="more1">
                                    {{$job->description}}
                                </p>
                                <div class="spec-div1 mt10">
                                    <p class="bold-500">Desired Skills :</p>
                                    <p class="color666"> {!! implode(",",$job->skills()->pluck('name')->toArray()); !!} </p>
                                </div>
                                <div class="spec-div1 spec-div12 ">
                                    <p class="bold-500">Benefits:</p>
                                    <p class="color666"> {{$job->benifits}}</p>
                                </div>

                            </div>

                            <div class="data-job-row3">
                                <div class="heading-job-div01">
                                    <h3>Submissions Remaining <span class="number-sub1">({{($job->upload_limit - $job->total_applied)}})</span></h3>
                                </div>
                                <div class="listing-divjob-3">
                                    <div class="data-row-div2 job3-div1">
                                        <h5>Steve Smith </h5>
                                        <div class="content-row3">
                                            <p class="p-blk1">Experience: 3 Years <span class="verticle-line">|</span> PHP, MySQL, Wordpress</p>
                                            <p class="p-blk1">5th january 2018, 10:10AM <span class="verticle-line">|</span> Submitted by <a href="agency-company-view-profile.html"
                                                    class="link-blue">Agency Name</a></p>
                                        </div>
                                        <div class="action-jobdiv">
                                            <a href="agency-upload-resume-form.html"><button class="btn btn-action2 btn-review2">Upload Resume</button></a>
                                        </div>
                                    </div>
                                    <div class="data-row-div2 job3-div1">
                                        <h5>Steve Smith </h5>
                                        <div class="content-row3">
                                            <p class="p-blk1">Experience: 3 Years <span class="verticle-line">|</span> PHP, MySQL, Wordpress</p>
                                            <p class="p-blk1">5th january 2018, 10:10AM <span class="verticle-line">|</span> Submitted by <a href="agency-company-view-profile.html"
                                                    class="link-blue">Agency Name</a></p>
                                        </div>
                                        <div class="action-jobdiv">
                                            <a href="agency-upload-resume-form.html"><button class="btn btn-action2 btn-review2">Upload Resume</button></a>
                                        </div>
                                    </div>
                                    <div class="data-row-div2 job3-div1">
                                        <h5>Steve Smith </h5>
                                        <div class="content-row3">
                                            <p class="p-blk1">Experience: 3 Years <span class="verticle-line">|</span> PHP, MySQL, Wordpress</p>
                                            <p class="p-blk1">5th january 2018, 10:10AM <span class="verticle-line">|</span> Submitted by <a href="agency-company-view-profile.html"
                                                    class="link-blue">Agency Name</a></p>
                                        </div>
                                        <div class="action-jobdiv">
                                            <a href="agency-upload-resume-form.html"><button class="btn btn-action2 btn-review2">Upload Resume</button></a>
                                        </div>
                                    </div>
                                    <div class="data-row-div2 job3-div1">
                                        <h5>Steve Smith </h5>
                                        <div class="content-row3">
                                            <p class="p-blk1">Experience: 3 Years <span class="verticle-line">|</span> PHP, MySQL, Wordpress</p>
                                            <p class="p-blk1">5th january 2018, 10:10AM <span class="verticle-line">|</span> Submitted by <a href="agency-company-view-profile.html"
                                                    class="link-blue">Agency Name</a></p>
                                        </div>
                                        <div class="action-jobdiv">
                                            <a href="agency-upload-resume-form.html"><button class="btn btn-action2 btn-review2">Upload Resume</button></a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


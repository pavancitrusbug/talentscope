@extends('layouts.frontend')
@section('content')


    <div class="agency-box">
        <div class="heading-div">
            <h4>Uploaded Resumes ({{$job->title}})</h4>
        </div>
        @if ($resumes)
            @foreach ($resumes as $key => $resume)

                <div class="view-candidate-inner">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="data-row-div2 data-row-agency">
                                <h5>{{$resume->candidate_name}}</h5>

                                <div class="content-row3">
                                    <p class="p-blk1">Experience : {{$resume->job_experience}}  | {{$job->job_code}}</p>
                                    <p class="p-blk1">{{$resume->candidate_education}}</p>

                                </div>
                                <div class="bottom-div">
                                    {{ Form::open(array('url'=>url("agent/delete-resume/$resume->id"),'method' => 'DELETE','files'=>'true'))}}
                                <a href="{{url('agent/edit-resume')}}/{{$resume->id}}" class="linka1">
                                    <img src="{!! asset('front-end/images/icons/edit.svg') !!}" class="img-fluid img-icon11">
                                </a>

                                    {{-- <button type="submit" class="linka1">
                                        <img src="{!! asset('front-end/images/icons/delete-button.svg') !!}" class="img-fluid img-icon11">
                                    </button> --}}

                                    <input type="image" src="{!! asset('front-end/images/icons/delete-button.svg') !!}" alt="delete" class=" img-fluid img-icon11">

                                {{ Form::close() }}
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 text-right-1">
                            <div class="data-row-div2 data-row-agency">

                                <a href="#" class="btn-custom btn-red-1">Upload Resume</a>

                            </div>
                        </div>


                    </div>
                </div>
            @endforeach

        @endif

        {{-- <div class="view-candidate-inner">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="data-row-div2 data-row-agency">
                        <h5>Mark Anderson </h5>

                        <div class="content-row3">
                            <p class="p-blk1">UI/UX Designer | 5+ year experience | #TS4565</p>
                            <p class="p-blk1">BS - IT: Software Analysis and Development</p>

                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 text-right-1">
                    <div class="data-row-div2 data-row-agency">

                        <a href="#" class="btn-custom btn-red-1">Upload Resume</a>

                    </div>
                </div>

            </div>
        </div> --}}

    </div>

@endsection

@section('footerExtra')

<script>
    $(document).ready(function() {

    });

</script>
@endsection
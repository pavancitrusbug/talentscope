 <div class="col-lg-12 col-md-12 col-sm-12 full12-div float-left clearfix">
            <div class="row">
                <div class="width-100">
                    <div class="listing-div2 my-jobs1 my-jobs2">  
                        <div class="heading-div">
                            <h4>Jobs</h4>
                        </div>
                        <div class="body-listing">	
                            @if(isset($jobs))
                                @foreach($jobs as $job) 
                                    <div class="data-row-div">
                                        <h5>{{$job->title}} </h5><span class="id-span"> #TS4565</span>
                                        @if($job->fixed_fee != null)
                                            <label class="label1">Fixed Fee: {{$job->fixed_fee}}!</label>
                                        @else
                                            <label class="label1">Placement Fee: {{$job->placement_fee}}%!</label>
                                        @endif
                                        
                                        <p class="p-blk1">Experience: min. {{$job->job_experience[0]->name}} | {!! implode(",",$job->skills()->pluck('name')->toArray()); !!}</p>
                                        <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on {{\Carbon\Carbon::parse($job->created_at)->format('dS F Y')}}</p> 
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>	
            </div>			
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 five-div float-left clearfix">
            <div class="row">
                <div class="request-agency-div">
                    <div class="listing-div3">  
                        <div class="heading-div">
                            <h4>Employees</h4>
                        </div>
                        <div class="body-listing02">
                            @if(isset($employees))
                                    @foreach($employees as $employee) 
                                        <div class="fullrow-div clearfix">
                                            <div class="toprow-div clearfix">
                                                <div class="col-div11">
                                                    <div class="agency-thumb1">
                                                        @if(isset($employee->profile_image))           
                                                            <img src="{!! asset('/user/'.$employee->profile_image) !!}" class="img-fluid agency-img1" alt="user-img">
                                                        @else
                                                            <img class="img-fluid agency-img1" src="{!! asset('front-end/images/userdefault.png')!!}" alt="user-img">
                                                        @endif
                                                    </div>
                                                    <div class="title-div"><h5>{{$employee->name}}</h5><a href="#" class="link01">{{$employee->website}}</a></div>
                                                </div>
                                                <div class="col-div1 col-exep">
                                                    <div class="title-div1"><h6>experience :</h6><p>{{$employee->year_founded}}</p></div>
                                                </div>
                                                
                                                <div class="col-div1 col-div3">
                                                    <div class="title-div1"><h6>LOCATION :</h6><p><i class="fas fa-map-marker-alt locationi"></i>{{$employee->address}}</p></div>
                                                </div>
                                                <div class="col-div1 col-div4">
                                                    <div class="title-div1"><h6>STATUS :</h6><label class="pending-label">PENDING</label></div>
                                                </div>
                                            </div>	
                                            <div class="bottomhover-row">
                                                <div class="width-75per">
                                                    <a href="#" class="link-blk1">VIEW EMPLOYER PROFILE</a>
                                                </div>
                                                <div class="width-25per">
                                                    <button class="btn btn-action btn-primary send_request" data_value="{{$employee->id}}">
                                                        Send Request
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                        </div>
                        
                    </div>
                </div>	
            </div>			
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 five-div float-left clearfix">
            <div class="row">
                <div class="request-agency-div">
                    <div class="listing-div3">  
                        <div class="heading-div">
                            <h4>Agents</h4>
                        </div>
                        <div class="body-listing02">
                            @if(isset($agents))
                                    @foreach($agents as $agent) 
                                        <div class="fullrow-div clearfix">
                                            <div class="toprow-div clearfix">
                                                <div class="col-div11">
                                                    <div class="agency-thumb1">
                                                        @if(isset($agent->profile_image))           
                                                            <img src="{!! asset('/user/'.$agent->profile_image) !!}" class="img-fluid agency-img1" alt="user-img">
                                                        @else
                                                            <img class="img-fluid agency-img1" src="{!! asset('front-end/images/userdefault.png')!!}" alt="user-img">
                                                        @endif
                                                    </div>
                                                    <div class="title-div"><h5>{{$agent->name}}</h5><a href="#" class="link01">{{$agent->website}}</a></div>
                                                </div>
                                                <div class="col-div1 col-exep">
                                                    <div class="title-div1"><h6>experience :</h6><p>{{$agent->year_founded}}</p></div>
                                                </div>
                                                
                                                <div class="col-div1 col-div3">
                                                    <div class="title-div1"><h6>LOCATION :</h6><p><i class="fas fa-map-marker-alt locationi"></i>{{$agent->address}}</p></div>
                                                </div>
                                                <div class="col-div1 col-div4">
                                                    <div class="title-div1"><h6>STATUS :</h6><label class="pending-label">PENDING</label></div>
                                                </div>
                                            </div>	
                                            <div class="bottomhover-row">
                                                <div class="width-100per">
                                                    <a href="#" class="link-blk1">VIEW EMPLOYER PROFILE</a>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                        </div>
                    
                    </div>
                </div>	
            </div>			
        </div>
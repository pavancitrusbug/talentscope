@extends('layouts.frontend')

@section('content')
@if($errors->any())
<div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong></strong> {{$errors->first()}}
</div>
@endif
    <div class="col-lg-12 col-md-12 col-sm-12 full-div clearfix">
        <div class="row">
            <div class="counting-box">
                <div class="count-box1">
                    <div class="icon-thumb">
                        <img src="{!! asset('front-end/images/icons/round1.png') !!}" class="img-fluid img-round1">
                    </div>
                    <div class="count-content">
                        <p>Active Engagements</p>
                        <h2 class="counter">21</h2>
                    </div>
                </div>
                <div class="count-box1">
                    <div class="icon-thumb">
                        <img src="{!! asset('front-end/images/icons/round2.png') !!}" class="img-fluid img-round1">
                    </div>
                    <div class="count-content">
                        <p>Talent Hired</p>
                        <h2>34</h2>
                    </div>
                </div>
                <div class="count-box1">
                    <div class="icon-thumb">
                        <img src="{!! asset('front-end/images/icons/round3.png') !!}" class="img-fluid img-round1">
                    </div>
                    <div class="count-content">
                        <p>Agencies Connected</p>
                        <h2>25</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-7 col-md-12 col-sm-12 seven-div float-left clearfix">
        <div class="row">
            <div class="padding-right10">
                <div class="listing-div">
                    <div class="heading-div">
                        <h4>Job Listing</h4>
                        <div class="float-right"><a href="{{url("agent/upload-resume")}}" class="btn btn-primary btn-red">POST A JOB</a></div>
                    </div>
                    <div class="body-listing">
                        @if (!empty($jobs))
                            @foreach ($jobs as $job)
                                <div class="data-row-div">
                                    <h5>{{$job->title}} </h5><span class="id-span"> {{$job->job_code}}</span>
                                    <div class="right-absolute1">
                                    <a class="btn btn-action1 btn-view" href="{{url('agent/job-detail')}}/{{$job->id}}">View</a>
                                        <span class="fee-block">Fee: {{($job->placement_fee)?$job->placement_fee."%":$job->fixed_fee}}</span>
                                    </div>
                                    <p class="p-blk1">{{$job->location}} </p>
                                    <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on {{\Carbon\Carbon::parse($job->created_at)->format('dS F Y')}}</p>
                                    <p class="pr-10"><i class="far fa-clock timeri"></i>Expiring on {{\Carbon\Carbon::parse($job->expiry_date)->format('dS F Y')}}</p>

                                </div>
                                {{-- <div class="data-row-div">
                                    <h5>{{$job->title}} </h5><span class="id-span"> {{$job->job_code}}</span>
                                    <a class="btn btn-transparent" href="{{url("agent/upload-resume")}}/{{$job->id}}">Recruit Talent</a>

                                    <p class="p-blk1">Experience: min. {{$job->job_experiance}} | {!! implode(",",$job->skills()->pluck('name')->toArray()); !!}</p>
                                    <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on {{\Carbon\Carbon::parse($job->created_at)->format('dS F Y')}}</p>
                                    <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on 3rd August 2018</p>

                                    <div class="bottom-div">
                                        <a href="{{url("agent/uploaded-resume")}}/{{$job->id}}" class="btn btn-primary btn-red btn-red1"><span class="number-span">{{$job->total_applied}}</span>Resumes Submitted</a>
                                        <a href="#" class="linka1"><img src="{!! asset('front-end/images/icons/edit.svg') !!}" class="img-fluid img-icon11"></a>
                                        <a href="#" class="linka1"><img src="{!! asset('front-end/images/icons/delete-button.svg') !!}" class="img-fluid img-icon11"></a>
                                    </div>
                                </div> --}}
                            @endforeach

                        @endif
                        {{-- <div class="data-row-div">
                            <h5>Wordpress developer </h5><span class="id-span"> #TS4565</span>
                            <a class="btn btn-transparent" href="{{url("agent/upload-resume")}}" >Recruit Talent</a>

                            <p class="p-blk1">Experience: min. 2 years | PHP, MySQL, Wordpress</p>
                            <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on 3rd August 2018</p>
                            <p class="pr-10"><i class="far fa-clock timeri"></i>Posted on 3rd August 2018</p>

                            <div class="bottom-div">
                                <a href="#" class="btn btn-primary btn-red btn-red1"><span class="number-span">5</span>Resumes Submitted</a>
                                <a href="#" class="linka1"><img src="{!! asset('front-end/images/icons/edit.svg') !!}" class="img-fluid img-icon11"></a>
                                <a href="#" class="linka1"><img src="{!! asset('front-end/images/icons/delete-button.svg') !!}" class="img-fluid img-icon11"></a>
                            </div>
                        </div> --}}


                    </div>
                    <div class="view-all-div">
                        <a href="#" class="view-alllink">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-12 col-sm-12 five-div float-left clearfix">
        <div class="row">
            <div class="padding-left10">
                <div class="listing-div2">
                    <div class="heading-div">
                        <h4>Recent Submissions</h4>
                    </div>
                    <div class="body-listing">
                        <div class="data-row-div2">
                            <h5>Steve Smith </h5>
                            <label class="label2">Pending</label>
                            <span class="id-span"> #TS4565</span>

                            <div class="icon-div01">
                                <a href="#" class="iconalink"> <img src="{!! asset('front-end/images/icons/eye.svg') !!}" class="img-fluid img-i01 w25"> </a>

                            </div>

                            <p class="p-blk1">Experience: 3 Years | PHP, MySQL, Wordpress</p>
                            <p>Submitted by <a href="#" class="link-blue">Agency Name</a></p>

                        </div>


                    </div>
                    <div class="view-all-div">
                        <a href="#" class="view-alllink">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 five-div float-left clearfix">
        <div class="row">
            <div class="request-agency-div">
                <div class="listing-div3">
                    <div class="heading-div">
                        <h4>My Rating</h4>
                    </div>
                    @if (count($reviews) > 0)
                        @php
                            $avg_star = 0;
                            $total_ratting = 0;
                            $total_one_star = 0;
                            $total_two_star = 0;
                            $total_three_star = 0;
                            $total_four_star = 0;
                            $total_five_star = 0;
                            $total_ratting = 0;
                            $most_popular_ratting = null;
                            if ($reviews) {
                                $total_star = $reviews->sum('ratting');
                                $total_ratting = $reviews->count();
                                $avg_star = round($total_star/$total_ratting,1);
                                $total_one_star = $reviews->where('ratting',1)->count();
                                $total_two_star = $reviews->where('ratting',2)->count();
                                $total_three_star = $reviews->where('ratting',3)->count();
                                $total_four_star = $reviews->where('ratting',4)->count();
                                $total_five_star = $reviews->where('ratting',5)->count();
                                $ratting_array = array(
                                    '1' => $total_one_star,
                                    '2' => $total_two_star,
                                    '3' => $total_three_star,
                                    '4' => $total_four_star,
                                    '5' => $total_five_star,
                                );
                                $ratting_percentages = array(
                                    '1'=> round((($ratting_array[1]*100)/$total_star),2),
                                    '2'=> round(((($ratting_array[2]*2)*100)/$total_star),2),
                                    '3'=> round(((($ratting_array[3]*3)*100)/$total_star),2),
                                    '4'=> round(((($ratting_array[4]*4)*100)/$total_star),2),
                                    '5'=> round(((($ratting_array[5]*5)*100)/$total_star),2),
                                );
                                $value = max($ratting_array);
                                $most_popular_ratting = array_search($value, $ratting_array);
                                $total_popular_star = $value*$most_popular_ratting;
                                $popular_percentage = round((($total_popular_star*100)/$total_star),2);
                            }
                        @endphp

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="average-rating">
                                    <div class="rating">
                                        <h3>Average Rating</h3>
                                        <h3><span>{{$avg_star}}</span>
                                            <span class="rating-star">
                                            <i class="{{($avg_star >= 1)?'fas':'far'}} fa-star filled"></i>
                                                <i class="{{($avg_star >= 2)?'fas':'far'}} fa-star filled"></i>
                                                <i class="{{($avg_star >= 3)?'fas':'far'}} fa-star filled"></i>
                                                <i class="{{($avg_star >= 4)?'fas':'far'}} fa-star filled"></i>
                                                <i class="{{($avg_star >= 5)?'fas':'far'}} fa-star"></i>
                                            </span>
                                        </h3>
                                        <img src="{!! asset('front-end/images/rating-bar.png') !!}" class="img-fluid" />
                                        <h4><div class="line-bar"></div>Average Rating</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="chart-bar">
                                    <div class="rating">
                                            <h3>Most popular rating <span>{{$popular_percentage}} %</span> <span class="title-mobile">{{$most_popular_ratting}} <i class="fas fa-star filled"></i> <span>{{max($ratting_array)}}</span> ratings<span></h3>
                                    </div>
                                    <div class="chart-box">
                                        <div class="chart">
                                            <div class="split div-0"><p>0%</p></div>
                                            <div class="split div-15"><p>15%</p></div>
                                            <div class="split div-50"><p>50%</p></div>
                                            <div class="split div-75"><p>75%</p></div>
                                            <div class="split div-100"><p>100%</p></div>
                                        </div>
                                        <div class="horizontal-bar">
                                                <div class="h-bar h-bar-{{$ratting_percentages[5]}}"><span>5 <i class="fas fa-star filled"></i></span></div>
                                                <div class="h-bar h-bar-{{$ratting_percentages[4]}}"><span>4 <i class="fas fa-star filled"></i></span></div>
                                                <div class="h-bar h-bar-{{$ratting_percentages[3]}}"><span>3 <i class="fas fa-star filled"></i></span></div>
                                                <div class="h-bar h-bar-{{$ratting_percentages[2]}}"><span>2 <i class="fas fa-star filled"></i></span></div>
                                                <div class="h-bar h-bar-{{$ratting_percentages[1]}}"><span>1 <i class="fas fa-star filled"></i></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="view-all-div">
                        <a href="#" class="view-alllink">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

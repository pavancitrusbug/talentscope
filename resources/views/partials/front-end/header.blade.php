<header class="top-header">
		<div class="header-div">
			<div class="container">
				<div class="row">

					<div class="col-lg-2 col-md-3 col-sm-6 clearfix">
						<div class="logo-thumb">
                        <a href="{{url('')}}"><img src="{!! asset('front-end/images/logo.png') !!}" class="img-fluid img-logo"></a>
						</div>
					</div>

					<div class="col-lg-10 col-md-9 col-sm-6 clearfix">
						<div class="top-nav-div float-right">

							<div class="top-nav2 top-nav25">
								<div class="search-box">
									<div class="input-group">
										<form id="search_form" action="{{ url('/agent/search_data') }}" method="POST" style="display:block" >
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="text" class="form-control" placeholder="Search" id="Search" name="search_data" style="display:block">
										</form>
										
										<div class="input-group-append">
											<span class="input-group-text"><i class="fas fa-search"></i></span>
										</div>
									</div>
								</div>
								<div class="notification-box">
									<div class="dropdown dropdown01">
                                        <button id="notification_button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="{!! asset('front-end/images/icons/bell.svg') !!}" class="img-fluid img-icon01">
                                            {{-- <span class="badge badge-primary" id="count-notification">{{auth()->user()->unreadNotifications->count()}}</span> --}}
                                        </button>
										<ul class="dropdown-menu" >
											<li id="notification_bar">
												<div class="header-dropdown clearfix">
													<h2>Notifications</h2>
                                                </div>

												{{-- <div class="media custom-media clearfix">
													<div class="media-left-img"><img class="img-fluid" src="{!! asset('front-end/images/userdefault.png') !!}" alt="user-img"></div>
													<div class="media-body">
														<h5 class="mt-0">User Name</h5>
														<p>Lorem ipsum dolor... </p>
													</div>
												</div> --}}
											</li>
										</ul>
									</div>
								</div>
								<div class="user-dropdown">
									<div class="dropdown">
										<button class="btn btn-primaryuser dropdown-toggle" type="button" data-toggle="dropdown">
											<div class="user-img">
                                                @if(isset(Auth::user()->profile_image))

                                                    <img src="{!! asset('/user/'.Auth::user()->profile_image) !!}" class="img-fluid userimg1" alt="user-img">
                                                @endif

                                            </div>
											<span class="username-span">{{Auth::user()->name}}</span>

                                            <img src="{!! asset('front-end/images/icons/arrow-down-2.svg') !!}" class="img-fluid img-downarrow">
										</button>
										<ul class="dropdown-menu">
											<li><a href="{{ url('/employess/my_profile') }}">My Profile</a></li>
											<li><a href="{{ url('/employess/change_password') }}">Change Password</a></li>
											<li>
                                                <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Log out</a>
                                            </li>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                            </form>
										</ul>
									</div>
								</div>

							</div>

							<div class="top-nav1 top-nav75">
								<div id="cd-shadow-layer"></div>
								<div class="nav-m-bar"><a href="#" onclick="openNav()" class="opennav" data-placement="bottom" title="" data-original-title="Menu">
									<i class="menu-bars"></i></a>
								</div>
								<div class="nav-div clearfix" id="mySidenav" >
									<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
									<ul class="nav ullist-inline" id="nav-res" >
										<li><a href="#" class="active">My jobs</a></li>
										<li><a href="#">My agencies</a> </li>
										<li><a href="#">MESSAGES</a></li>
										<li><a href="#">Report</a></li>
										@if(Auth::user()->can('access.job.create'))
											<li><a href="{{url('/employess/jobs')}}" class="btn btn-primary btn-primaryc1">POST A JOB</a></li>
										@endif
									</ul>
								</div>

							</div>


						</div>
					</div>

				</div>
			</div>
		</div>
	</header>

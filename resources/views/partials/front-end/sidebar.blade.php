<div class="col-lg-3 col-md-4 col-sm-12 clearfix">
    <div class="profile1-div">
        <div class="user-thumb">
      
                @if(isset(Auth::user()->company->logo))
                   <img src="{!! asset('/company/'.Auth::user()->company->logo) !!}" class="img-fluid com-profile" alt="user-img" id="image_data">
                @else
                    <img class="img-fluid com-profile" src="{!! asset('front-end/images/userdefault.png') !!}" alt="user-img">
                @endif

           
        </div>
        <div class="upload-div">
        <form enctype="multipart/form-data" id="fupForm" >
            {{csrf_field()}}

            <div class="file-upload">
                <label for="upload" class="file-upload__label">Upload Picture</label>
                <input id="upload" class="file-upload__input" type="file" name="file_upload" >
            </div>
            </form>
        </div>
        @if(isset(Auth::user()->company->name))
            <h4>{{Auth::user()->company->name}}</h4>
        @endif
    </div>
    <div class="company-link-div">
        <h3>Website</h3>
        <div class="link-div1">
        @if(isset(Auth::user()->company->website))   
            <a href="{{Auth::user()->company->website}}" class="link-red"><img src="{!! asset('front-end/images/icons/network-connection.svg') !!}" class="img-fluid img-iconlink01">
                <span class="fl-left">
                       {{Auth::user()->company->website}}
                </span>
            </a>
            @endif
        </div>
    </div>
</div>
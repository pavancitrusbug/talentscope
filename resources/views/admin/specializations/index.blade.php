@extends('layouts.backend')

@section('title','Specialization List')


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Specialization List</h3>
                    @if(Auth::user()->can('access.specializations.create'))
                        <a href="{{ url('/admin/specializations/create') }}" class="btn btn-success btn-sm pull-right"
                            title="Add New Specialization">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Status</th>                   
                            <th>Action</th>
                        </tr> 
                        </thead>
                        <tbody>
                            @foreach($specializations as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <input type='checkbox' value='{{$item->status}}' item_id='{{$item->id}}' class='item_status' name='user_status' {{($item->status == 1)?'checked':''}}>
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/specializations/' . $item->id) }}" title="View Role">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View </button>
                                        </a>
                                            @if(Auth::user()->can('access.specializations.edit'))
                                                <a href="{{ url('/admin/specializations/' . $item->id . '/edit') }}"
                                                    title="Edit Specialization">
                                                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </button>
                                                </a>
                                            @endif

                                            @if(Auth::user()->can('access.specializations.delete'))
                                                <form method="POST" action="{{ url('/admin/specializations'.'/'.$item->id) }}" accept-charset="UTF-8" style="display:inline" class = "confirm_delete">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-xs" title="Delete User"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></button>
                                                </form>
                                                
                                            @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>   
                </div>
            </div>   
        </div>
    </div>
@endsection
@section('footerExtra')
    <script>
        $( document ).ready(function() {
           
            var el = document.getElementsByClassName('.confirm_delete');

            $('.item_status').on('click',function() {
             
                var user_id = $(this).attr('item_id');
                var status=$(this).val();
                $.ajax({
                    data:{ id : user_id,status:status, _token: '{!! csrf_token() !!}'},
                    url: '{!! route('SpecializationsStatusChangeController') !!}',
                    type: 'POST',
                    dataType: 'json',

                    success:function(data)
                    {
                      if(data.success)
                      {
                        swal("Status updated success fully!");
                      }
                      else 
                      {
                        swal("Status not updated success fully!");
                      }
                    }
                });
            });
        
        });

    </script>

@endsection


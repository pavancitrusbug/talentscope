<div class="box-body">

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-4 control-label">
                <span class="field_compulsory">*</span>Name
            </label> 
            <div class="col-md-6">
                {!! Form::text('name', isset($user) ? $user->name : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('name','<p class="help-block">:message</p>') !!}
            </div>
        </div>

        @if(Auth::user()->can('access.role.edit'))
            <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
                <label for="role" class="col-md-4 control-label">
                    <span class="field_compulsory">*</span>Role
                </label>
                <div class="col-md-6">
                    {!! Form::select('roles',$roles,isset($user_roles) ? $user_roles : '', ['class' => 'form-control select2', 'multiple' =>false ,'id'=> 'role' ]) !!}
                    {!! $errors->first('roles','<p class="help-block">:message</p>') !!}
                </div>
            </div>
        @endif

        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="col-md-4 control-label">
                <span class="field_compulsory">*</span>Email
            </label> 
            <div class="col-md-6">
            {!! Form::email('email', isset($user) ? $user->email : '', ['class' => 'form-control','disabled' => isset($user) ? true : false ]) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
      
        <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
            <label for="contact" class="col-md-4 control-label">Contact</label> 
            <div class="col-md-6">
            {!! Form::text('contact', isset($user) ? $user->contact : '' , ['class' => 'form-control']) !!}
            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
  
        <div class="form-group{{ $errors->has('profile_image') ? ' has-error' : ''}}">
            <label for="contact" class="col-md-4 control-label">profile image</label> 
            <div class="col-md-6">
            {!! Form::file('profile_image',null, ['class' => 'form-control']) !!} 
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
            </div>
            <div class="col-md-6">
            @if(isset($user) && isset($user->profile_image))
                @if( file_exists(public_path('/user/'.$user->profile_image) )  )
                    <img  src="{{url('/user/'.$user->profile_image)}}" width="125" height="100" alt="{{ $user->profile_image }}"/>  
                @endif          
            @endif 
            </div>
            {!! $errors->first('profile_image', '<p class="help-block with-errors">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                <label for="gender" class="col-md-4 control-label">Gender</label> 
                <div class="col-md-6">
                {!! Form::radio('gender', 'male' , ['class' => 'form-control']) !!} Male
                {!! Form::radio('gender', 'female' , ['class' => 'form-control']) !!} Female
                {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                </div>
        </div> 

        <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
                <label for="company_name" class="col-md-4 control-label">Company Name</label> 
                <div class="col-md-6">
                {!! Form::text('company_name', isset($company) ? $company->name : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_name','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_logo') ? 'has-error' : ''}}">
                <label for="company_logo" class="col-md-4 control-label">Company Logo</label> 
                <div class="col-md-6">
                {!! Form::file('company_logo',null, ['class' => 'form-control']) !!} 
                {!! $errors->first('company_logo','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
            </div>
            <div class="col-md-6">
            @if(isset($company) && isset($company->logo))
                @if( file_exists(public_path('/company/'.$company->logo) )  )
                    <img  src="{{url('/company/'.$company->logo)}}" width="125" height="100" alt="{{ $company->logo }}"/>  
                @endif          
            @endif 
            </div>
            
        </div>
        <div class="form-group {{ $errors->has('company_address') ? 'has-error' : ''}}">
                <label for="company_address" class="col-md-4 control-label">Company Address</label> 
                <div class="col-md-6">
                {!! Form::textarea('company_address','jknkjn', ['id' => 'company_address', 'rows' => 6, 'cols' => 54, 'style' => 'resize:none']) !!}
                {!! $errors->first('company_address','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_city') ? 'has-error' : ''}}">
                <label for="company_city" class="col-md-4 control-label">Company City</label> 
                <div class="col-md-6">
                {!! Form::select('company_city',$city,isset($company) ? $company->city_id : '', ['class' => 'form-control select2', 'multiple' =>false ,'id'=> 'role' ]) !!}
                {!! $errors->first('company_city','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_state') ? 'has-error' : ''}}">
                <label for="company_state" class="col-md-4 control-label">Company State</label> 
                <div class="col-md-6">
                {!! Form::select('company_state',$state,isset($company) ? $company->state_id : '', ['class' => 'form-control select2', 'multiple' =>false ,'id'=> 'role' ]) !!}
                {!! $errors->first('company_state','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_country') ? 'has-error' : ''}}">
                <label for="company_country" class="col-md-4 control-label">Company Country</label> 
                <div class="col-md-6">
                {!! Form::select('company_country',$country,isset($company) ? $company->country_id : '', ['class' => 'form-control select2', 'multiple' =>false ,'id'=> 'role' ]) !!}
                {!! $errors->first('company_country','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_pincode') ? 'has-error' : ''}}">
                <label for="company_pincode" class="col-md-4 control-label">Company Pincode</label> 
                <div class="col-md-6">
                {!! Form::text('company_pincode',  isset($company) ? $company->pin_code : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_pincode','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_contact') ? 'has-error' : ''}}">
                <label for="company_contact" class="col-md-4 control-label">Company Contact</label> 
                <div class="col-md-6">
                {!! Form::text('company_contact',  isset($company) ? $company->contact : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_contact','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_email_address') ? 'has-error' : ''}}">
                <label for="company_email_address" class="col-md-4 control-label">Company Email-Address</label> 
                <div class="col-md-6">
                {!! Form::text('company_email_address',isset($company) ? $company->email_address : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_email_address','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_about') ? 'has-error' : ''}}">
                <label for="company_about" class="col-md-4 control-label">Company About</label> 
                <div class="col-md-6">
                {!! Form::textarea('company_about', isset($company) ? $company->about : '', ['id' => 'company_about', 'rows' => 6, 'cols' => 54, 'style' => 'resize:none']) !!}
                {!! $errors->first('company_about','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_website') ? 'has-error' : ''}}">
                <label for="company_website" class="col-md-4 control-label">Company Website</label> 
                <div class="col-md-6">
                {!! Form::text('company_website',  isset($company) ? $company->website : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_website','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_year_founded') ? 'has-error' : ''}}">
                <label for="company_year_founded" class="col-md-4 control-label">Company Year Founded</label> 
                <div class="col-md-6">
                {!! Form::text('company_year_founded',  isset($company) ? $company->year_founded : '', ['class' => 'form-control']) !!} 
                {!! $errors->first('company_year_founded','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_specialization_id') ? 'has-error' : ''}}">
                <label for="company_specialization_id" class="col-md-4 control-label">Company Specialization</label> 
                <div class="col-md-6">
                {!! Form::select('company_specialization_id',$specialization,isset($company) ? $company->specialization()->get()->pluck('id') : '', ['class' => 'form-control specialization','name'=>'specialization[]']) !!}
                {!! $errors->first('company_specialization_id','<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('company_verified') ? 'has-error' : ''}}">
                <label for="company_verified" class="col-md-4 control-label">Company Verified</label> 
                <div class="col-md-6">
                {!! Form::radio('company_verified', '1' , ['class' => 'form-control']) !!} Yes
                {!! Form::radio('company_verified', '0' , ['class' => 'form-control']) !!} NO
                {!! $errors->first('company_verified', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
    
    
</div>

@section('footerExtra')
@push('js')
<script>
        $( document ).ready(function() {
    
            $(".specialization").select2({
                tags: true,
                multiple: true,
                tokenSeparators: [',', ' ']
            });

            var roles = $('#role').val();
            
            if(roles != '' || roles != null){
  
                    if(roles == "Installers"){
                        $(".installer_fields").show()  
                    }else{
                        $(".installer_fields").hide()
                    }
            }   
        });
        $('#role').on('change',function(){
            var roles = $(this).val();
      
            if( roles != null){
                    if(roles == "Installers"){
                        $(".installer_fields").show()
                    }else{
                        $(".installer_fields").hide()
                    } 
            }else{
                $(".installer_fields").hide()
            }
   
       });
</script>
@endpush
@endsection
